﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">

	
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid2">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapseds"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="${pageContext.request.contextPath}/Restaurantmain/main.do">맛잘알</a>

			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<form class="navbar-form navbar-left" action="searchByNameType.do">
					<input name=RestNameList type="text" class="form-control"
						placeholder="맛집 입력" value=""> <input
						class="btn btn-xs btn-default" type="submit" value="검색">
				</form>

				<form class="navbar-form navbar-left" action="searchByrestType.do">
					<input name=RestType type="text" class="form-control"
						placeholder="타입 입력" value=""> <input
						class="btn btn-xs btn-default" type="submit" value="검색">
				</form>

				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">
						<c:choose>
						<c:when test="${empty userid }">
						로그인 하러가기
						</c:when>
						<c:otherwise>
						${userid}님 환영합니다
						</c:otherwise>
						</c:choose>
						<span class="caret"></span></a>
						<div class="nav navbar-nav navbar-left">
							<form id=login.do>

								${users} 
								<a>${users.getId}</a>
								 <a>${users.getPassword}</a>
								 <a>${users.getMaster}</a>

								<c:choose>
									<c:when test="${empty userid}">

										<ul class="dropdown-menu">
											<li><a
												href="${pageContext.request.contextPath}/User/login.do">로그인</a></li>
											<li><a
												href="${pageContext.request.contextPath}/User/insertUser.do">회원가입</a></li>
										</ul>
									</c:when>
									<c:otherwise>
										<ul class="dropdown-menu">
											<li><a
												href="${pageContext.request.contextPath}/User/logout.do">로그아웃</a></li>
											<li><a
												href="${pageContext.request.contextPath}/User/mypage.do?id=${userid}">MyPage</a></li>
									</ul>

									</c:otherwise>
								</c:choose>
							</form>
						</div></li>
				</ul>
			</div>
		</div>
	</nav>
	<div id="wrap">
		<div id="header" role="header">
			<div class="container">
				<div class="header">
					<!-- //헤더 메뉴 -->
					<div class="header-tit">
						<h1>맛잘알</h1>
						<br> <a href="#">WebProject Team3</a>
					</div>
					<!-- //헤더 타이틀 -->
					<div class="header-icon">
						<a href="#" class="icon1"><span class="ir_pm">icon1</span></a> <a
							href="#" class="icon2"><span class="ir_pm">icon2</span></a> <a
							href="#" class="icon3"><span class="ir_pm">icon3</span></a>
					</div>
					<!-- //헤더 아이콘 -->
				</div>
			</div>
		</div>
		<!-- //header -->


		<div id="contents">
			<div id="cont_nav">
				<div class="container">
					<h2 class="ir_su">전체 메뉴</h2>
					<div class="nav2">
						<div>
							<h3>
								<div class="dropdown">
									<a href="" class="dropdown-toggle" data-toggle="dropdown"
										role="button" aria-haspopup="true" aria-expanded="false">여행별
										맛집<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/travelall.do">전체</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/sinbo.do">신부동</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/dojung.do">두정</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/bungchen.do">병천</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/beakseok.do">백석대학교</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/sangmyung.do">상명대학교</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/hoseo.do">호서대학교</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/dankook.do">단국대학교</a></li>
									</ul>
								</div>
							</h3>

						</div>
						<div>
							<div>
								<h3>
									<div class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown"
											role="button" aria-haspopup="true" aria-expanded="false">계절별
											맛집<span class="caret"></span>
										</a>
										<ul class="dropdown-menu">
											<li><a
												href="${pageContext.request.contextPath}/Restaurantmain/spring.do">봄</a></li>
											<li><a
												href="${pageContext.request.contextPath}/Restaurantmain/summer.do">여름</a></li>
											<li><a
												href="${pageContext.request.contextPath}/Restaurantmain/autumn.do">가을</a></li>
											<li><a
												href="${pageContext.request.contextPath}/Restaurantmain/winter.do">겨울</a></li>
										</ul>
									</div>
								</h3>
							</div>
						</div>
						<div>
							<h3>
								<div class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"
										role="button" aria-haspopup="true" aria-expanded="false">테마별
										맛집 <span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/parttv.do">TV
												추천</a></li>
										<li><a
											href="${pageContext.request.contextPath}/Restaurantmain/partblog.do">Blog
												추천</a></li>
									</ul>
								</div>
							</h3>
						</div>
						<div class="last">
							<h3>
								<li><a href="${pageContext.request.contextPath}/NoticeBoard/noticeboard.do">게시판</a></li>
							</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="cont_tit">
			<div class="container">
				<div class="tit">
					<h2>&ldquo;맛잘알 되기!&rdquo;</h2>
					<a href="#" class="btn"><span class="ir_pm">전체메뉴</span></a>
				</div>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/slick.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/lightgallery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/lightgallery-all.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/custom.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
</body>

</html>