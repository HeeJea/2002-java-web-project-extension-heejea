<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>마이페이지</title>
<%@ include file="/views/layout/common.jsp"%>

<style type="text/css">
body {
	background-color: #ffffff;
}

.login-header {
	max-width: 400px;
	padding: 15px 29px 25px;
	margin: 0 auto;
	background-color: #ffbb00;
	color: white;
	text-align: center;
	-webkit-border-radius: 15px 15px 0px 0px;
	-moz-border-radius: 15px 15px 0px 0px;
	border-radius: 15px 15px 0px 0px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.login-footer {
	max-width: 400px;
	margin: 0 auto 20px;
	padding-left: 10px;
}

.form-signin {
	max-width: 400px;
	padding: 29px;
	margin: 0 auto 20px;
	background-color: #ffe8aa;
	-webkit-border-radius: 0px 0px 15px 15px;
	-moz-border-radius: 0px 0px 15px 15px;
	border-radius: 0px 0px 15px 15px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.form-signin .form-signin-heading, .form-signin .checkbox {
	margin-bottom: 15px;
}

.form-signin input[type="text"], .form-signin input[type="password"] {
	font-size: 16px;
	height: auto;
	margin-bottom: 15px;
	padding: 7px 9px;
}

.form-btn {
	text-align: center;
	padding-top: 20px;
}

.form-control {
	display: block;
	width: 70%;
	height: 43px;
	padding: 10px 15px;
	font-size: 15px;
	line-height: 1.428571429;
	color: #2c3e50;
	vertical-align: middle;
	background-color: #ffffff;
	background-image: none;
	border: 1px solid #dce4ec;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	-webkit-transition: border-color ease-in-out 0.15s, box-shadow
		ease-in-out 0.15s;
	transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
}

.btn {
	display: inline-block;
	padding: 10px 15px;
	margin-bottom: 0;
	font-size: 15px;
	font-weight: normal;
	line-height: 1.428571429;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	cursor: pointer;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

.btn-warning {
	color: #ffffff;
	background-color: #E07110;
	border-color: #E07110;
}

.btn-default {
	color: #ffffff;
	background-color: #bbbbbb;
	border-color: #bbbbbb;
}
</style>
</head>
<body>
	<div class="container">
		<center>
			<hr>
			<br> <br>

			<div class="login-header">
				<div>
					<img
						src="${pageContext.request.contextPath}/resources/image/맛잘알 로고1.png"
						width=100 height=100>
				</div>
				<h1>마이페이지</h1>
			</div>

			<div class="form-signin">

				<table width=300 border=0 cellpadding=2 cellspacing=1>
					<tr>
						<td style="font-size: 1.5em; font-weight: bold;">이름:</td>
						<td style="font-size: 1.5em;">${User.name}</td>
					</tr>
					<tr>
						<td style="font-size: 1.5em; font-weight: bold;">아이디 :</td>
						<td style="font-size: 1.5em;">${User.id}</td>
					</tr>
					<tr>
						<td style="font-size: 1.5em; font-weight: bold;">이메일 :</td>
						<td style="font-size: 1.5em;">${User.email}</td>
					</tr>
					<tr>
						<td style="font-size: 1.5em; font-weight: bold;">지역 :</td>
						<td style="font-size: 1.5em;">${User.area}</td>
						<br>
						<br>

					</tr>
				</table>
				<br> <br> <input class="btn btn-large btn-default"
					type="button" value="회원수정"
					onclick="location.href='updateUser.do?id=${User.id}'" /> <input
					class="btn btn-large btn-default" type="button" value="회원삭제"
					onclick="location.href='deleteUser.do'" />


			</div>
			<br>



		</center>
	</div>

</body>
</html>