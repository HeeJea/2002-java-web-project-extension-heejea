﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>유저삭제버튼</title>
</head>


<body>


</body>


</html>

<%@ include file="/views/layout/common.jsp"%>

<style type="text/css">
        body {
            background-color: #ffffff;
        }
        .login-header {
            max-width: 400px;
            padding: 15px 29px 25px;
            margin: 0 auto;
            background-color: #ffbb00;
            color: white;
            text-align: center;
            -webkit-border-radius: 15px 15px 0px 0px;
            -moz-border-radius: 15px 15px 0px 0px;
            border-radius: 15px 15px 0px 0px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .login-footer {
            max-width: 400px;
            margin: 0 auto 20px;
            padding-left: 10px;
        }
        .form-signin {
            max-width: 400px;
            padding: 29px;
            margin: 0 auto 20px;
            background-color: #ffe8aa;
            -webkit-border-radius: 0px 0px 15px 15px;
            -moz-border-radius: 0px 0px 15px 15px;
            border-radius: 0px 0px 15px 15px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 15px;
        }
        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }
        .form-btn {
            text-align: center;
            padding-top: 20px;
        }
        .form-control {
    display: block;
    width: 70%;
    height: 43px;
    padding: 10px 15px;
    font-size: 15px;
    line-height: 1.428571429;
    color: #2c3e50;
    vertical-align: middle;
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #dce4ec;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
}
.btn {
    display: inline-block;
    padding: 10px 15px;
    margin-bottom: 0;
    font-size: 15px;
    font-weight: normal;
    line-height: 1.428571429;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    }
    .btn-warning {
    color: #ffffff;
    background-color: #E07110;
    border-color: #E07110;
}
.btn-default {
    color: #ffffff;
    background-color: #bbbbbb;
    border-color: #bbbbbb;
}

    </style>
<title>맛잘알 회원 삭제</title>
</head>

<body>
	<br>
	<br>
	<br>
	<center>
		<div class="login-header">
			<div>
		<img src="${pageContext.request.contextPath}/resources/image/맛잘알 로고1.png" width=100 height=100>
	</div>

			<h1>정말 나갈꺼야?</h1>
			<br>
			<h1>한번 더 생각해봐</h1>
		</div>

		<form action="deleteUser.do" method="post" class="form-signin">
			아이디 <input class="form-control" type="text" id="id" name="id"
				placeholder="삭제 할 ID">
			<button class="btn btn-large btn-warning" type="submit">회원삭제</button>
		</form>
	</center>
</body>
</html>