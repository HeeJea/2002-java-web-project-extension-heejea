<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>

    <title>페이지이름</title>
    <%@ include file="/views/layout/common.jsp"%>
</head>
<body>


<!-- Container ======================================================================================= -->
<div class="container">
    <div class="table-responsive">
    	<h4>게시판</h4>
    	<hr>
         <table class="table table-striped table-bordered table-hover">
          <colgroup>
           <col width="100"/>
           <col width="*"/>
           <col width="120"/>
           <col width="70"/>
           <col width="50"/>
          </colgroup>
          <thead>
           <tr>
            <th class="text-center">번호</th>
            <th class="text-center">제목</th>
            <th class="text-center">작성일</th>
            <th class="text-center">작성자</th>
            <th class="text-center">조회</th>
           </tr>
          </thead>
         <tbody>
          <c:choose>
           <c:when test="${empty boardlist}">
		    <tr>
		      <th colspan="5" class="text-center">게시물이 존재하지 않습니다.</th>
		    </tr>
           </c:when>
           <c:otherwise>
            <c:forEach var="list" items="${boardlist}">
			 <tr>
			  <td class="text-center">${list.bno}</td>
			  <td class="text-center">
			  <a href="${pageContext.request.contextPath}/NoticeBoard/find.do?bno=${list.bno}">${list.title}</a>
			  </td>
			   <td class="text-center">
			    <fmt:formatDate value="${list.regDate}" pattern="yyyy-MM-dd"/>
			   </td>
			  <td class="text-center">${list.authorName}</td>
			  <td class="text-center">${list.viewcnt}</td>
			 </tr>
		    </c:forEach>
           </c:otherwise>
          </c:choose>
         </tbody>
        </table>
       </div>    
       
       <div class="text-right">
                <a href="${pageContext.request.contextPath}/NoticeBoard/regist.do">
                	<button type="button" class="btn btn btn-warning">등록</button>
                </a>
                
            </div> 
      

</div>

</body>
</html>