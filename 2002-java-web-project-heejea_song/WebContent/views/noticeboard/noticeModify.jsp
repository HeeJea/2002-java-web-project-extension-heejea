<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>

<title>페이지이름</title>
<%@ include file="/views/layout/common.jsp"%>
</head>
<body>
	<!-- Container ======================================================================================= -->
	<div class="container">
		<div class="table-responsive">
			<div class="col-sm-9 col-lg-9">
				<form
					action="${pageContext.request.contextPath}/NoticeBoard/modify.do"
					enctype="multipart/form-data" method="POST">
					<fieldset>
						<div class="panel panel-default">
							<input type="hidden" name="bno" value="${post.bno}">
							<div class="panel-heading">
								<h4><input type="text" name="title" class="form-control" value="${post.title}"></h4>
							</div>
							<div class="panel-body">
								<div class="post">
									<div class="write_info">
										<span class="name">${post.authorName}</span> <span
											class="date"><span class="_val">${post.regDate}</span></span>

									</div>
								</div>
								<div class="form-group">
									<textarea class="form-control" name="contents" rows="5">${post.contents}</textarea>
								</div>
								업로드 된 파일
								<c:forEach var="photoId" items='${photoId}'>

									<input type='hidden' id='photoIds' name='photoIds'
										value='${photoId.photoId}'>
									<p>
										<input type='hidden' id='files2' name='files2'
											value='${photoId.photoName}'>${photoId.photoName} <a
											href='#this' class='btn' id='delete' name='delete'>삭제</a>
									</p>
								</c:forEach>
								<div id="fileDiv"></div>
								<a href="#this" id="add" class="btn btn-primary">파일 추가하기</a>
							</div>
						</div>

						<div class="text-center">
							<button type="submit" class="btn btn-primary">저장</button>
							<a href="${pageContext.request.contextPath}/NoticeBoard/find.do?bno=${post.bno}" class="btn btn-default">취소</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

	</div>

	<script type="text/javascript">
		$(document).ready(function() {

			$("#add").on("click", function(e) {
				e.preventDefault();
				fn_fileAdd();
			})

			$("a[name='delete']").on("click", function(e) {
				e.preventDefault();
				fn_fileDelete($(this));
			})

		})
		function fn_fileAdd() {
			var str = "<p>"
					+ "<input type='file' id='files' name='files'>"
					+ "<a href='#this' class='btn' id='delete' name='delete'>삭제</a>"
					+ "</p>";
			$("#fileDiv").append(str);
			$("a[name='delete']").on("click", function(e) { //삭제 버튼
				e.preventDefault();
				fn_fileDelete($(this));
			});
		}
		function fn_fileDelete(obj) {
			obj.parent().remove();
		}
	</script>
</body>
</html>