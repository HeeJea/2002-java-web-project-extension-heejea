<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>

<title>페이지이름</title>
<%@ include file="/views/layout/common.jsp"%>
</head>
<body>
	<!-- Container ======================================================================================= -->
	<div class="container">
		<div class="col-sm-9 col-lg-9">
			<div>
				<h3>게시글 등록</h3>
			</div>

			<div class="table-responsive">
				<div class="well">

					<form
						action="${pageContext.request.contextPath}/NoticeBoard/regist.do?userName=user"
						class="bs-example form-horizontal" enctype="multipart/form-data"
						method="POST">

						<fieldset>
							<div class="form-group">
								<label class="col-lg-2 control-label">제목</label>

								<div class="col-lg-10">
									<input type="text" name="Title" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label class="col-lg-2 control-label">내용</label>

								<div class="col-lg-10">
									<textarea class="form-control" name="Contents" rows="3"
										id="textArea"></textarea>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									첨부파일
									<div id="fileDiv"></div>
									
									<a href="#this" id="add" class="btn btn-primary">파일 추가하기</a>
								</div>
							</div>
							<button type="submit" class="btn btn-primary">확인</button>
							<button type="reset" class="btn btn-default">취소</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>



	</div>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#add").on("click", function(e) {
				e.preventDefault();
				fn_fileAdd();
			})

			$("a[name='delete']").on("click", function(e) {
				e.preventDefault();
				fn_fileDelete($(this));
			})

		})
		function fn_fileAdd() {
			var str = "<p>"
					+ "<input type='file' multiple='multiple' id='files' name='files'>"
					+ "<a href='#this' class='btn' id='delete' name='delete'>삭제</a>"
					+ "</p>";
			$("#fileDiv").append(str);
			$("a[name='delete']").on("click", function(e) { //삭제 버튼
				e.preventDefault();
				fn_fileDelete($(this));
			});
		}
		function fn_fileDelete(obj) {
			obj.parent().remove();
		}
	</script>
</body>
</html>