<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>

    <title>페이지이름</title>
    <%@ include file="/views/layout/common.jsp"%>
</head>
<body>


<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="jumbotron">
                    <h2>게시판</h2> <!-- db에서 이름 받아오는걸로 수정할예정-->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="#">홈</a></li>
                    <li><a href="#">게시판</a></li> <!-- db에서 이름 받아오는걸로 수정할예정-->
                </ol>
            </div>
        </div>
    </div>
</header>

<!-- Container ======================================================================================= -->
<div class="container">
    <div class="col-sm-9 col-lg-9">
            <div>
                <h3>게시글 등록</h3>
            </div>
          
            <div class="table-responsive">
                <div class="well">
                    <form action="${pageContext.request.contextPath}/NoticeBoard/update.do"
                          class="bs-example form-horizontal" method="POST">
                       
                        <fieldset>
                            
                            <div class="form-group">
                                <label class="col-lg-2 control-label">${comment.writer}</label>

                                <div class="col-lg-10">
                                <input type="hidden" name="bno" value="${comment.bno}">
                                <input type="hidden" name="cno" value="${comment.cno}">
                                    <textarea class="form-control" name="substance" rows="3" id="textArea"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="submit" class="btn btn-primary">확인</button>
                                    <button type="reset" class="btn btn-default">취소</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>   
       
      

</div>

</body>
</html>