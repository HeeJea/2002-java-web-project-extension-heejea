<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<head>

<title>페이지이름</title>
<%@ include file="/views/layout/common.jsp"%>
</head>
<body>

	<br>
	<br>
	<!-- Container ======================================================================================= -->
	<div class="container">
		<div class="table-responsive">
			<div class="col-sm-9 col-lg-9">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>${post.title}</h4>
					</div>
					<div class="panel-body">
						<div class="post">

							<strong>${post.authorName}</strong> &nbsp;<span
								class="text-muted">${post.regDate}</span> &nbsp;<span
								class="text-muted">${post.viewcnt} 읽음</span> <a
								href="${pageContext.request.contextPath}/NoticeBoard/modify.do?bno=${post.bno}"
								class="glyphicon glyphicon-cog pull-right" style="padding: 10px">수정</a>
							<a
								href="${pageContext.request.contextPath}/NoticeBoard/remove.do?bno=${post.bno}"
								class="glyphicon glyphicon-trash pull-right"
								style="padding: 10px">삭제</a>
						</div>
						<br>

						<p style="padding: 20px">${post.contents}</p>

						<c:forEach var="photoIds" items='${photoIds}'>
							<div>
								<img
									src="getphoto/<c:out value='${post.bno}'/>/<c:out value='${photoIds.photoId}'/>.do"
									width=70% height=auto><br>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>

		

			<!-- 댓글창 -->


			<form
				action="/team3_final_project/NoticeBoard/comment.do"
				class="bs-example form-horizontal" method="POST">
				<input type="hidden" name="bno" value="${post.bno}">
				<div class="form-group">
					<div class="col-lg-10">
						<textarea class="form-control" name="substance" rows="3"
							id="textArea"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-10 col-lg-offset-2">
						<button type="submit" class="btn btn-primary">등록</button>
						<a
					href="${pageContext.request.contextPath}/NoticeBoard/noticeboard.do">
					<button type="button" class="btn btn-default">목록</button>
				</a>
					</div>
				</div>
			</div>
			</form>
			




			<h4>전체댓글</h4>
			<hr>


			<c:choose>
				<c:when test="${empty comment}">
            	댓글이 없습니다.<br>첫번째 댓글을 남겨주세요.
            </c:when>
				<c:otherwise>
					<c:forEach var="list" items="${comment}">
						<div style="line-height: 150%">
							<strong>${list.writer}</strong> <a
								href="/team3_final_project/NoticeBoard/update.do?cno=${list.cno}&bno=${list.bno}"
								class="glyphicon glyphicon-cog pull-right" style="padding: 10px">수정</a>
							<a
								href="/team3_final_project/NoticeBoard/delete.do?cno=${list.cno}&bno=${list.bno}"
								class="glyphicon glyphicon-trash pull-right"
								style="padding: 10px">삭제</a>
							<div>
								${list.substance}<br> ${lsit.date}
								<hr>
							</div>
						</div>
					</c:forEach>
				</c:otherwise>
			</c:choose>

		</div>
	</div>

	</div>

</body>
</html>