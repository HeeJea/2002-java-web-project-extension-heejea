<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html lang="ko">
<head>
<%@ include file="/views/layout/common.jsp"%>
<style type="text/css">
body {
	padding-top: 100px;
	padding-bottom: 40px;
	background-color: #ecf0f1;
}

.login-header {
	max-width: 400px;
	padding: 15px 29px 25px;
	margin: 0 auto;
	background-color: #2c3e50;
	color: white;
	text-align: center;
	-webkit-border-radius: 15px 15px 0px 0px;
	-moz-border-radius: 15px 15px 0px 0px;
	border-radius: 15px 15px 0px 0px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.login-footer {
	max-width: 400px;
	margin: 0 auto 20px;
	padding-left: 10px;
}

.form-signin {
	max-width: 400px;
	padding: 29px;
	margin: 0 auto 20px;
	background-color: #fff;
	-webkit-border-radius: 0px 0px 15px 15px;
	-moz-border-radius: 0px 0px 15px 15px;
	border-radius: 0px 0px 15px 15px;
	-webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	-moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
	box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
}

.form-signin .form-signin-heading, .form-signin .checkbox {
	margin-bottom: 15px;
}

.form-signin input[type="text"], .form-signin input[type="password"] {
	font-size: 16px;
	height: auto;
	margin-bottom: 15px;
	padding: 7px 9px;
}

.form-btn {
	text-align: center;
	padding-top: 20px;
}
</style>

</head>
<body>
	<div class="container">

		<!-- header -->
		<div class="login-header">
			<h2 class="form-signin-heading">맛잘알</h2>
		</div>

		<!-- form -->
		<form
			action="${pageContext.request.contextPath}/create/createinsertUser.do"
			method="POST" class="form-signin">
			<tr>
				아이디 :
				<input type="text" class="form-control" name="uid"
					placeholder="아이디(이메일)" required>
			</tr>
			<tr>
				비밀번호 :
				<input type="password" class="form-control" name="pwd"
					placeholder="비밀번호" required>
			</tr>
			<tr>
				이메일 :
				<input type="text" class="form-control" name="email"
					placeholder="이메일" required>
			</tr>
			<div class="row form-btn">
				<button class="btn btn-large btn-warning" type="submit">회원가입</button>
				<button class="btn btn-large btn-warning" type="reset">취소</button>
			</div>
		</form>

		<!-- footer -->
		<div class="login-footer">
			<p>© NamooSori 2015.</p>
		</div>
	</div>
</body>
</html>