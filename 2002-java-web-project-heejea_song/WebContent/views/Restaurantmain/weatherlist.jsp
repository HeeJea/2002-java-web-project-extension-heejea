<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">

<title>맛잘알</title>
<%@ include file="/views/layout/common.jsp"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">



</head>
<body>

	<div class="container-fluid">
		<!-- //cont_tit -->
		<!-- Page Content -->
		<h1 class="my-4">
			${weatherList.weathertype} 맛집 추천 리스트
		</h1>

		<div class="row">
			<c:forEach var="restweatherlist" items="${RestaruantweatherList}">
			<div class="col-lg-2 col-md-2 col-sm-4 mb-2">
				<div class="card h-100">
					<a href="detail.do?foodId=${restweatherlist.foodId}"> <img src="getphoto4/<c:out value='${restweatherlist.foodId}'/>.do" width=250 height=250></a>
					<div class="card-body">
						<h4 class="card-title">
							<a href="detail.do?foodId=${restweatherlist.foodId}">식당 명 : ${restweatherlist.name}</a>
							<p>식당 이름: ${restweatherlist.number}</p>
							<p>식당 위치 : ${restweatherlist.location}</p>
							<br>
							<div class="card-footer">
			               	<a href="detail.do?foodId=${restweatherlist.foodId}" class="btn btn-primary">${restweatherlist.name} 자세히 보기</a>
			             	</div>
							<p class="card-text"></p>
					</div>
				</div>
			</div>
			</c:forEach>
		</div>		
			<!-- /.container -->

		</div>

	
</body>
</html>

