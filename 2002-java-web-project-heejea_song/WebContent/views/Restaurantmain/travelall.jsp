<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="ko">
<head>

<title>Web_Team3_먹으러갈래</title>
<%@ include file="/views/layout/common.jsp"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">
</head>
<body>

	<header>


		<div class="container-fluid">
			<table class="table table-striped table-bordered table-hover">
				<colgroup>
					<col width="70" />
					<col width="100" />
					<col width="130" />
					<col width="*" />
					<col width="70" />
				</colgroup>
				<script>
					var loctemper = [];
					var lngtemper = [];
					var nametemper = [];
					var markers = [];
					var contentbody = [];
					var inter = 1;
					var counter = "${fn:length(travelList)}";
					var j = 1;
				</script>
				<tbody>
					<c:forEach var="travel" items="${travelList}" varStatus="sts">
						<tr>
							<script>
								var i = "${sts.count}";
								loctemper[i] = "${travel.latitude}";
								lngtemper[i] = "${travel.longitude}";
								nametemper[i] = "${travel.name}";
								contentbody[i] = '<p>식당 이미지 : '
										+ '<a href="detail.do?foodId=${travel.foodId}"> <img src="getphoto3/<c:out value='${travel.foodId}'/>.do" width=50 height=50></a></p>'
										+ '<p>식당명 :'
										+ '<a href=detail.do?foodId=${travel.foodId}>'
										+ '${travel.name}</a></p>'
										+ '<p>식당위치 :${travel.location}</p>'
										+ '<p>식당위도 :${travel.latitude}</p>'
										+ '<p>식당경도 :${travel.longitude}</p>';
							</script>

						</tr>
					</c:forEach>



					<!-- Portfolio Item Heading -->
					<h1 class="my-12">
						여행 <small></small>
					</h1>

					<!-- Portfolio Item Row -->
					<div class="row">

						<div class="col-md-12">
							<div>
								여행 목적지 입력 : <input id="address" type="textbox"
									value="여행지 입력해주세요"> <input id="submit" type="button"
									value="검색">
							</div>
							<hr>
							<div id="map" style="width: 100%; height: 1750px;"></div>
							<body onload="slocation()">
							<script async="" defer=""
								src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTOF_tkCg4k25XkhjTeSrd2WQX_y5Xp0I&callback=initMap"
								type="text/javascript">
								
							</script>
						</div>
					</div>
					<!-- /.row -->

					<!-- Related Projects Row -->
					<br>
					<hr>
					<h3 class="my-12">여행 맛집 사진</h3>
					<hr>
					<div class="row">
					<c:forEach var="travel" items="${travelList}" varStatus="sts">
						<div class="col-lg-3 col-md-4 mb-4">
			           <div class="card h-100">
			             <a href="detail.do?foodId=${travel.foodId}"> <img src="getphoto3/<c:out value='${travel.foodId}'/>.do" width=250 height=250>
			             <br>
			             <div class="card-body">
			               <h4 class="card-title">맛집 이름 : ${travel.name}</h4>
			               <br>
			               <p class="card-text">맛집 위치 : ${travel.location}</p>
							<br>
			             <div class="card-footer">
			               <a href="detail.do?foodId=${travel.foodId}" class="btn btn-primary">자세히 보기</a>
			             </div>
   			             </div>
			           </div>
			         </div>
				</c:forEach>			
					</div>

				</tbody>
			</table>
		</div>
	</header>


	<script>
		// window.alert(contentbody[4]);
	</script>


	<script>
		// var heatmap;
		// var heatMapData = [];

		// funxtion geocode(addressList){
		//	var addressList = new Array();

		function slocation() {

			var markerArray = [];
			var Slur = {
				lat : 36.8321706359134,
				lng : 127.135840399887
			};
		
			var markerMaxWidth = 300;
			var markerMaxWidth2 = 300;

			var mapOptions = {
				zoom : 14,
				mapTypeId : google.maps.MapTypeId.ROADMAP,
				center : Slur

			}
			var geocoder = new google.maps.Geocoder();

			document.getElementById('submit').addEventListener('click',
					function() {
						inputAddress(geocoder, map);
					});

			map = new google.maps.Map(document.getElementById('map'),
					mapOptions);

			//			var map2 = new google.maps.Map(document.getElementById('map2'), {
			//		         zoom: 15,
			//		         center: Slur
			//		       });

			var image = {
				url : 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
			//			          size: new google.maps.Size(20, 20),
			///			          origin: new google.maps.Point(0, 0),
			//		          anchor: new google.maps.Point(50, 52)
			};

			for (var i = 1; i <= counter; i++) {
				markerArray[i] = new google.maps.LatLng(loctemper[i],
						lngtemper[i]);
			}

			for (var i = 1; i <= markerArray.length; i++) {
				addMarker();
			}

			function inputAddress(geocoder, resultsMap) {
				var address = document.getElementById('address').value;
				geocoder
						.geocode(
								{
									'address' : address
								},
								function(results, status) {
									if (status === 'OK') {
										resultsMap
												.setCenter(results[0].geometry.location);

										var marker = new google.maps.Marker(
												{
													map : resultsMap,
													position : results[0].geometry.location
												});
									} else {
										alert('Geocode was not successful for the following reason: '
												+ status);
									}
								});
			}

			function addMarker() {

				var marker = new google.maps.Marker({
					position : markerArray[inter],
					map : map,
					icon : image,
					draggable : false,
					title : nametemper[inter]
				});

				markers.push(marker);

				var infowindow = new google.maps.InfoWindow({
					content : contentbody[inter]
				});

				google.maps.event.addListener(marker, "click", function() {
					infowindow.open(map, marker);
				});
				inter++;
			}

			google.maps.event.addDomListener(window, "load", initMap)

		}
	</script>

</body>
</html>