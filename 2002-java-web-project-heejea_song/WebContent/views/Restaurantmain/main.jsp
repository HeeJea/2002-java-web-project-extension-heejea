<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<title>Web_Team3_먹으러갈래</title>
<%@ include file="/views/layout/common.jsp"%>

<style type="text/css">
#content {
	margin: 20px;
	padding: 10px;
	background: #FFBB00;
}

#rank-list a {
	color: #f6fdff;
	text-decoration: none;
}

#rank-list a:hover {
	text-decoration: underline;
}

#rank-list {
	overflow: hidden;
	width: 160px;
	height: 20px;
	margin: 0;
}

#rank-list dt {
	display: none;
}

#rank-list dd {
	position: relative;
	margin: 0;
}

#rank-list ol {
	position: absolute;
	top: 0;
	left: 0;
	margin: 0;
	padding: 0;
	list-style-type: none;
}

#rank-list li {
	height: 20px;
	line-height: 20px;
}
</style>

</head>

<body>
	<!-- Container ======================================================================================= -->
	<div class="container">
		<div class="soar" align="center">
			<h3>&ldquo;오늘의 HOT10 게시 글&rdquo;</h3>
		</div>
		<div id="content">
			<dl id="rank-list">
				<dd>
					<ol>
						<c:forEach var="soararticles" items="${soararticles}">
							<li><a
								href="${pageContext.request.contextPath}/NoticeBoard/find.do?bno=${soararticles.bno}">${soararticles.title}</a></li>
						</c:forEach>
					</ol>
				</dd>
			</dl>
		</div>

		<div class="container">
			<div class="soar">
				<h3>&ldquo;오늘의 HOT10 맛집&rdquo;</h3>
			</div>
			<div class="ban">
				<c:forEach var="soarphotoIds" items='${soarphotoIds}'>
					<a href="detail.do?foodId=${soarphotoIds.foodId}"><img
						src="getphoto/<c:out value='${soarphotoIds.foodId}'/>.do"
						width=280 height=200></a>
				</c:forEach>
			</div>
		</div>
	</div>


	<c:set var="pagecount" value="39" />
	<c:forEach items="${restaurantList}" varStatus="sts">
		<c:set var="pagcount" value="${sts.count}" />
	</c:forEach>

	<div class="container-fluid">
		<div class="soar">
			<h3>&ldquo;천안 맛집은?&rdquo;</h3>
		</div>
		<br>
		<table class="table table-striped table-bordered table-hover">

			<c:forEach var="restaurant" items="${restaurantList}" varStatus="sts"
				begin="0" end="${pagecount}">
				<div class="clearfix visible-sm-block"></div>
				<div class=”clearfixvisible-xs-block“></div>
				<div class="col-lg-3 col-md-4 col-sm-4 mb-3">
					<div class="card h-100">
						<a href="detail.do?foodId=${restaurant.foodId}"><img
							src="getphoto2/<c:out value='${restaurant.foodId}'/>.do"
							width=250 height=250></a>
						<div class="card-body">
							<h4 class="card-title"></h4>
							<p>번호 : ${restaurant.foodId}</p>
							<p>맛집 이름 : ${restaurant.name}</p>
							<p>맛집 번호 : ${restaurant.number}</p>
							<p>맛집 위치 : ${restaurant.location}</p>
							<p>맛집 유형 : ${restaurant.type}</p>
							<p>오픈 시간 : ${restaurant.open}</p>
							<div class="card-footer">
								<a href="detail.do?foodId=${restaurant.foodId}"
									class="btn btn-primary">${restaurant.name} 자세히 보기</a>
							</div>
							<br> <br>
							<p class="card-text"></p>
						</div>
					</div>
				</div>
			</c:forEach>

			<div align="center">
				<ul class="pagination justify-content-center">
					<c:forEach var="page" items="${pagelist}" varStatus="sts">
						<li class="page-item"><a class="page-link"
							href="pagelist.do?pageid=${page.pageid}">${sts.count}</a></li>
					</c:forEach>
				</ul>
			</div>
		</table>
	</div>


	<script
		src="${pageContext.request.contextPath}/resources/js/custom2.js"></script>

	<%@ include file="/views/layout/footer.jsp"%>
</body>
</html>