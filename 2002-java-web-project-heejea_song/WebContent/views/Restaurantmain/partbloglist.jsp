<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="EUC-KR">
<title>Web_Team3_먹으러갈래</title>
<%@ include file="/views/layout/common.jsp"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">
</head>

<body>

	<div class="container-fluid">

		<!-- Page Heading -->
		<hr>
		<h1 class="my-6" align="center">블로그 Top List 10</h1>
		<div class="row no-gutter">
			<div class="d-none d-md-flex col-md-4 col-lg-6"></div>

		</div>
	</div>

	<hr>

	<!-- Page Content -->
	<div class="container-fluid">

		<!-- Page Heading -->
		<h1 class="my-4">
			<small>Top 10</small>
		</h1>
		<br>
		
		
		<div class="row">
			<c:forEach var="list" items="${PartBlogList}" varStatus="sts" begin="0" end="9">
				<div class="col-lg-6 mb-4">
					<div class="card h-100">
						<a href="detail.do?foodId=${list.fid}"> <img src="getphoto5/<c:out value='${list.fid}'/>.do" width=500 height=250>
					<div class="col-lg-6 mb-4">
						<div class="card-body">
							<h4 class="card-title">블로그 이름 : ${list.blogname}</h4>
							<p class="card-text">블로그 제목 : ${list.blogtitle}</p>
							<p class="card-text">블로그 작성 날짜 : ${list.blogdata}</p>
							<p class="card-text">블로그 내용 : ${list.blogbody}</p>
						</div>
						<div class="card-footer">
							<a href="${list.bloglink}" target=“_blank" class="btn btn-primary" >블로그 확인하러 가기</a>						</div>
					</div>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>

