<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLEncoder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>페이지이름</title>
<%@ include file="/views/layout/common.jsp"%>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

</head>
<body>


	<!-- Container ======================================================================================= -->
	<div id="wrap">
	<c:choose>
	<c:when test="${empty usermaster}">
	</c:when>
	<c:otherwise>
	<div>
		<a class="btn btn-link btn-sm btn-block"
			href="${pageContext.request.contextPath}/Restaurantmain/photoadd.do?foodId=${detailName.foodId}">이미지
			넣기</a>
	</div>
	</c:otherwise>
	</c:choose>
	
		<div class="container">
			<div class="row" style="padding: 30px;">
				<div style="width: 50%; float: left;">

					<div class="slider">
						<c:forEach var="photoIds" items='${photoIds}'>
							<img
								src="getphoto/<c:out value='${detailName.foodId}'/>/<c:out value='${photoIds.photoId}'/>.do"
								width=200 height=300>
						</c:forEach>
					</div>
					<script>
						$(document).ready(function() {
							$('.slider').bxSlider();
						});
					</script>

				</div>
				<div style="width: 50%; float: right;">
					<h2 align="center">&ldquo;${detailName.name}&rdquo;</h2>
					<hr>
					<p align="center">주소 &nbsp;&nbsp;&nbsp;<br>${detailName.location}</p>
					<br>
					<p align="center">전화 &nbsp;&nbsp;&nbsp;<br>${detailName.number}</p>
					<br>
				</div>

				<div>
				<h4 align="center">상세정보</h4>
				<p align="center">영업시간 &nbsp;&nbsp;&nbsp;${detailName.open}</p>
				</div>
			</div>

			<br> <br>
			<div class="table-responsive">
				<h3><b>&ldquo;지도&rdquo;</b></h3>
				<hr>
				<div id="map" style="width: 1135px; height: 300px;"></div>
				<table width=1500 border=0 cellpadding=2 cellspacing=1
					bgcolor=#777777>
					<tr>
						<body onload="slocation()">
					<tr>
				</table>

				<script async="" defer=""
					src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTOF_tkCg4k25XkhjTeSrd2WQX_y5Xp0I&callback=initMap"
					type="text/javascript">
					
				</script>

			</div>
			<form name="temp1" id="tem">
				<div class="table-responsive">
					<h3><b>&ldquo;블로그 포스팅&rdquo;</b></h3>
					<hr>
					<table class="table table-striped table-bordered table-hover">
						<colgroup>
							<col width="100" />
							<col width="200" />
							<col width="120" />
							<col width="*" />
							<col width="50" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">블로그 이름</th>
								<th class="text-center">블로그 제목</th>
								<th class="text-center">블로그 작성 날짜</th>
								<th class="text-center">블로그 내용</th>
								<th class="text-center">블로그 주소</th>
								<th class="text-center">좋아요</th>
								<th class="text-center">싫어요</th>
								<th class="text-center">블로그 추천수</th>
							</tr>
						</thead>

						<script>
							var detailblogname = [];
							var detailblogtitle = [];
							var detailblogdata = [];
							var detailblogbody = [];
							var detailbloglink = [];
							var blogmainbody = [];
							var inter = 1;
							var counter = "${fn:length(postlist)}";
							var j = 1;
						</script>

						<!-- 맛집data넣을예정 -->
						<tbody>
							<c:forEach var="BlogList" items="${postlist}" varStatus="sts">
								<tr>
									<th class="text-center">${BlogList.blogname}</th>
									<th class="text-center"><a
										href="blogmodal.do?blogtitle=${BlogList.blogtitle}">${BlogList.blogtitle}</a></th>
										<th class="text-center">${BlogList.blogdata}</th>
										<th class="text-center">${BlogList.blogbody}</th>
										<th class="text-center"><a href="${BlogList.bloglink}"
										target=“_blank" class="btn btn-primary">블로그 보러가기</a></th>
						               	<th class="text-center"><a href="blogup.do?blogid=${BlogList.blogid}" class="btn btn-primary">좋아요</a></th>
						               	<th class="text-center"><a href="blogdown.do?blogid=${BlogList.blogid}" class="btn btn-primary">싫어요</a></th>
										<th class="text-center" id="like_result">${BlogList.blogcount}</th>
						         
										
										
									<script>
										var i = "${sts.count}";
										detailblogname[i] = "${BlogList.blogname}";
										detailblogtitle[i] = "${BlogList.blogtitle}";
										detailblogdata[i] = "${BlogList.blogdata}";
										detailblogbody[i] = "${BlogList.blogbody}";
										detailbloglink[i] = "${BlogList.bloglink}";
									</script>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>

	<script>
		function slocation() {

			var sinnumber1 = "${detailName.latitude}";
			var sinnumber2 = "${detailName.longitude}";

			number1 = parseFloat(sinnumber1);
			number2 = parseFloat(sinnumber2);

			var Slur = {
				lat : number1,
				lng : number2
			};
			var markerMaxWidth = 300;

			var map = new google.maps.Map(document.getElementById('map'), {
				zoom : 18,
				center : Slur
			});

			var marker = new google.maps.Marker({
				position : Slur,
				map : map,
				title : "${detailName.name}" 
			});

			var content = "${detailName.name}"

			
			var infowindow = new google.maps.InfoWindow({
				content : content
			});

			google.maps.event.addListener(marker, "click", function() {
				infowindow.open(map, marker);
			});

			google.maps.event.addDomListener(window, "load", initMap)
		}
	</script>



</body>
</html>