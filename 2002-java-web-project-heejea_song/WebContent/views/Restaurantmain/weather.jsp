<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">

<title>맛잘알</title>
<%@ include file="/views/layout/common.jsp"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">



</head>
<body>
		<!-- //cont_tit -->
		<!-- Page Content -->
		<div class="container-fluid">

			<!-- Page Heading -->
			<hr>
			<h1 class="my-6" align="center">
				천안 코스 여행 리스트
			</h1>
			<div class="row no-gutter">
    		<div class="d-none d-md-flex col-md-4 col-lg-6"></div>
   
	 			 </div>
			</div>
			
			<hr>
			<c:forEach var="weather" items="${weatherList}" varStatus="sts">
				<div class="row">
					<div class="col-md-6">
						<a href="#"> <img class="img-fluid rounded mb-3 mb-md-0"
							src="${pageContext.request.contextPath}/resources/image/${weather.weathertype}/${sts.count}.JPG" width=100% height=500 alt="">
						</a>
					</div>
					<div class="col-md-5">
						<p>
						<h2>Number ${sts.count}. ${weather.weathertype} 코스</h2>
						<br>
						</p>

						<p>
							<h2>여행지 이름 : ${weather.name}</h2>
						</p>
						<br>
						<p><h3>여행지 내용: ${weather.body}</h3></p>
						<br>
						<p><h3>여행지 위치 : ${weather.location}</h3></p>
						<br>
						<a class="btn btn-primary" href="weatherlist.do?weathertype=${weather.weathertype}">추천맛집 보러가기</a></p>
					</div>
				</div>
				<hr>
			</c:forEach>
			<hr>
		</div>
		<!-- /.container -->

	</div>
</body>
</html>

