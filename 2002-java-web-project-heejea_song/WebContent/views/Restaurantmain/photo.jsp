<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/views/layout/common.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>Web_Team3_먹으러갈래</title>
</head>
<body>
	<form
		action="${pageContext.request.contextPath}/Restaurantmain/photoadd.do"
		class="bs-example form-horizontal" enctype="multipart/form-data"
		method="POST">
		<fieldset>
			<input type="hidden" name="foodId" value="${foodId}">
			<div class="form-group" align="center">
				첨부파일
				<div id="fileDiv"></div>
				<input type='file' id='files' multiple='multiple' name='files'>

			</div>

			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2" align="center">
					<button type="submit" class="btn btn-primary">확인</button>
					<button type="reset" class="btn btn-default">취소</button>
				</div>
			</div>
		</fieldset>
	</form>
</body>
</html>