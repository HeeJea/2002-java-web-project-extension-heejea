<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="EUC-KR">
<title>Web_Team3_먹으러갈래</title>
<%@ include file="/views/layout/common.jsp"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/codingBooster.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/reset.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/style.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/slick.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/lightgallery.css">

<!-- 웹 폰트 -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nanum+Brush+Script"
	rel="stylesheet">
</head>

<body>

	<div class="container-fluid">

		<!-- Page Heading -->
		<hr>
		<h1 class="my-6" align="center">TV 맛집 리스트 Top 10</h1>
		<div class="row no-gutter">
			<div class="d-none d-md-flex col-md-4 col-lg-6"></div>

		</div>
	</div>

	<hr>

	<!-- Page Content -->
	<div class="container-fluid">

		<!-- Page Heading -->
		<h1 class="my-4">
		</h1>

		<div class="row">
			<c:forEach var="list" items="${PartList}" varStatus="sts" begin="0" end="9">
				<div class="col-lg-6 mb-4">
					<div class="card h-100">
						<a href="#"><img class="card-img-top" src="${pageContext.request.contextPath}/resources/image/파트/${list.restaurantname}.png" width="500" height="300" alt=""></a>
						<div class="card-body">
							<h4 class="card-title">식당 이름 : ${list.restaurantname}</h4>
							<p class="card-text">식당 위치 : ${list.location}</p>
							<p class="card-text">TV 프로그램 : ${list.tvprograme}</p>
							<p class="card-text">TV 방영일 : ${list.tvdate}</p>
							<p class="card-text">메인메뉴 : ${list.mainmenu}</p>
						</div>
										</div>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>

