癤풻reate database web_project_heejea;
use web_project_heejea;
-- SET FOREIGN_KEY_CHECKS = 0;
-- TRUNCATE TABLE tablename;
-- SET FOREIGN_KEY_CHECKS = 1;

create table restaurant(
FoodId int not null auto_increment primary key,
Name varchar(30) not null,

Rnumber varchar(15) not null,
Location varchar(60) not null,
Type varchar(20) not null,
Price varchar(20),
OpenTime varchar(20),
loc varchar(40),
lng varchar(40));

create table blogpost(
blogid int not null auto_increment,
blogname varchar(50),
blogtitle varchar(100),
blogdata varchar(50),
blogbody varchar(2048),
link varchar(400),
count int default 0,
fid int not null,
primary key(blogid),
foreign key (fid) references restaurant(FoodId));

 create table Comment_TB(
 Cno int not null,
 Substance varchar(50) not null,
 Writer varchar(30) not null,
 Date date,
 bno int not null,
 primary key(cno),
  foreign key(bno) references Notice_Post_TB(bno)
 );
 

create table restaurantphoto(
PhotoId int auto_increment,
FoodId int,
ResPhoto longblob,
primary key(PhotoId),
constraint fk_restaurantphoto foreign key (FoodId) references restaurant(FoodId));


create table travel(
travelid int not null auto_increment,
name varchar(20) not null,
location varchar(50),
loc varchar(40),
lng varchar(40),
fid int not null,
primary key(travelid),
foreign key (fid) references restaurant(FoodId));


create table soar(
 num int not null auto_increment primary key,
 foodId int not null,
 date date not null,
 count int default 0,
 foreign key(foodId) references restaurant(foodId)
 );

CREATE TABLE Notice_Post_TB 
(
	bno int not null Auto_Increment,
	Title varchar(200) not null,
    Contents varchar(4000),
    AuthorName varchar(30) not null,
    RegDate date,
    viewcnt int default 0,
	primary key(bno)
);

 create table weather(
 weatherid int not null primary key auto_increment,
 name varchar(30) not null,
 weathertype varchar(5) not null,
 location varchar(60) not null,
 body varchar(2048),
 loc varchar(40),
 lng varchar(40));
-- fid int not null,
-- foreign key(fid) references restaurant(FoodId));

create table Noticephoto(
photoId int not null auto_increment,
bno int not null,
noticePhoto mediumblob,
noticePhotoName varchar(50),
primary key(photoId)
);

insert into weather(Name, weathertype, Location, body, loc , lng) values ("遺곷㈃�쐞濡�踰싰퐙異뺤젣","遊�", "異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� 遺곷㈃ �슜�븫3湲� 10","遺곷㈃ �쐞濡� 踰싰퐙異뺤젣�뒗 泥쒖븞�떆 �룞�궓援� 遺곷㈃ �뿰異섎━�댘�슫�슜由ъ뿉 �씠瑜닿린�뒗 �빟 15km媛��웾�쓽 踰싰퐙湲몄뿉�꽌 留ㅻ뀈 遊� 4�썡寃쎌뿉 �뿴由щ뒗 異뺤젣�씠�떎. 二쇳뻾�궗�옣�씤 ���꽍珥덈벑�븰援� �씪���뿉�꽌�뒗 �뼞硫붿튂湲�, 誘쇱냽���씠, 踰싰퐙 �궗吏� �쟾�떆�쉶 �벑 �떎�뼇�븳 遺��� �뻾�궗媛� 吏꾪뻾�맂�떎." ,"36.788419" ,"127.257452");
insert into weather(Name, weathertype, Location, body, loc , lng) values ("泥쒖븞�궪嫄곕━怨듭썝","遊�", " 異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� 異⑹젅濡� 410","�꽌�슱, 寃쎌긽�룄, �쟾�씪�룄濡� 媛��뒗 湲몄씠 �굹�돇�뒗 泥쒖븞�궪嫄곕━瑜� 湲곕뀗�븯�뿬 留뚮뱺 怨듭썝�씠�떎. �씠怨녹뿉�꽌�뒗 �섏쿇�븞�궪嫄곕━�� �끂�옯留먮줈 �씡�닕�븳 �뒫�닔踰꾨뱾, �뻾沅곸쓽 臾몃（���뜕 �쁺�궓猷�, 怨좊젮 �떆���쓽 �궪猷〓룞 �궪痢듭꽍�깙 �벑�쓣 蹂� �닔 �엳�떎." , "36.783874" ,"127.168105");
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("泥쒗샇吏� �빞寃�","遊�", " 異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� �떒��濡� 119","泥쒗샇吏��뒗 泥쒖븞�떆瑜� �쓲瑜대뒗 泥쒖븞泥쒖쓽 蹂몄쨪湲곗씤 ���닔吏��씠�떎. 媛곸쥌 泥댁쑁 �떆�꽕怨� �떇臾쇰뱾濡� 媛��뱷�븳 �씠怨녹� 泥쒖븞 �궗�엺�뱾�쓽 �굹�뱾�씠 �삉�뒗 �궛梨� �옣�냼濡� �궗�옉諛쏅뒗 怨녹씠�떎. �궙肉먮쭔 �븘�땲�씪 �빞寃쎈룄 �븘由꾨떟湲곕줈 �쑀紐낇빐 ���닔吏� 二쇰��쓣 �뵲�씪 留덈젴�맂 �궛梨낅줈�뿉�뒗 諛ㅻ궙�뾾�씠 �궗�엺�뱾濡� 遺먮퉰�떎." , "36.837536" ,"127.168213"); 


insert into weather(Name, weathertype, Location, body, loc , lng)  values ("泥쒖븞醫낇빀�쑕�뼇愿�愿묒�(�썙�꽣�뙆�겕)","�뿬由�", "異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� �꽦�궓硫� 醫낇빀�쑕�뼇吏�濡� 200","以묐�沅� 理쒓퀬 �떆�꽕�쓣 �옄�옉�븯�뒗 泥쒖븞醫낇빀�쑕�뼇愿�愿묒��뿉�꽌�뒗 �썙�꽣�뙆�겕/�뒪�뙆, �떎�뼇�븳 而⑥뀎�쓽 �뙣諛�由� 由ъ“�듃 (��紐낅━議고듃)瑜� �씠�슜�븷 �닔 �엳�떎. 泥쒖븞12寃� 以� �젣8寃쎌뿉 �냽�븯�뒗 泥쒖븞醫낇빀�쑕�뼇愿�愿묒��뒗 �뿬由꾩뿉�뒗 �썙�꽣�뙆�겕, 寃⑥슱�뿉�뒗 �뒪�뙆瑜� �슫�쁺�븯誘�濡� �궗怨꾩젅 �궡�궡 利먭만 �닔 �엳�떎." , "36.757324", "127.224069" );
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("泥쒖븞 �삁�닠�쓽�쟾�떦","�뿬由�", "異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� �꽦�궓硫� �슜�썝由�","泥쒖븞�쓽 吏��뿭 臾명솕�삁�닠 諛쒖쟾�쓣 �쐞�빐 �끂�젰�븯�뒗 泥쒖븞 �삁�닠�쓽 �쟾�떦�� �떆誘쇱쓣 �쐞�븳 蹂듯빀臾명솕怨듦컙�씠�떎. �떎�뼇�븳 怨듭뿰, �쟾�떆瑜� 愿��엺�븷 �닔 �엳�쑝硫�, 臾명솕�꽱�꽣�룄 �슫�쁺�븯怨� �엳�떎." , "36.756911", "127.225359" );
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("�솉���슜怨쇳븰愿�", "�뿬由�"," 異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� �닔�떊硫� �옣�궛�꽌湲� 113","�솉���슜怨쇳븰愿��� 議곗꽑 �떆�� 理쒖큹濡� 吏��쟾�꽕怨� 吏�援� 援ы삎�꽕�쓣 二쇱옣�뻽�뜕 �떞�뿄 �솉���슜 �꽑�깮�쓽 �뾽�쟻�쓣 湲곕━怨좎옄 留덈젴�맂 怨녹씠�떎. �떎�뼇�븳 �슦二� 怨쇳븰 �쟾�떆臾�, 留앹썝寃�, 怨쇳븰泥댄뿕 �벑�쓣 �넻�빐 醫� �뜑 移쒓렐�븯寃� �슦二� 怨쇳븰�쓣 �젒�븷 �닔 �엳�룄濡� �룙�뒗�떎.","36.740006","127.295595");

insert into weather(Name, weathertype, Location, body, loc , lng)  values ("�룆由쎄린�뀗愿� �떒�뭾�굹臾� �댉湲�", "媛��쓣"," 異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� 紐⑹쿇�쓭 �룆由쎄린�뀗愿�濡� 1","�룆由쎄린�뀗愿��쓣 媛먯떥�뒗 �궛梨낅줈瑜� �뵲�씪 珥� 4.3km�쓽 �떒�뭾�굹臾� �댉湲몄씠 議곗꽦�릺�뼱 �엳�떎. �빟 8,000洹몃（�쓽 �떒�뭾�굹臾대줈 媛��뱷�븳 �씠怨녹� �꽦�씤 湲곗� �빟 1�떆媛� �댘 1�떆媛� 諛� �젙�룄 �냼�슂�릺�뒗 �궛梨낃만�씠�떎. �떒�뭾�씠 �븳李쎌씪 �떆湲�, 媛�議깃낵 �뿰�씤�씠 �븿猿� 嫄룰린 醫뗭� 怨녹씠�떎.","36.783762","127.223161");
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("�씎���졊異ㅼ텞�젣", "媛��쓣"," 泥쒖븞�궪嫄곕━怨듭썝 諛� �떆�궡 �씪�썝","�씎���졊異ㅼ텞�젣�뒗 �슦由� 誘쇱”�쓽 �섑씎�숈쓣 二쇱젣濡� �궓���끂�냼 紐⑤몢媛� �븿猿� �떊�굹寃� 利먭만 �닔 �엳�뒗 異뺤젣�씠�떎. 臾명솕泥댁쑁愿�愿묐� 吏��젙 理쒖슦�닔 異뺤젣濡� �꽑�젙�맂 �씠 異뺤젣�뒗 援��궡 理쒓퀬, 理쒕� 洹쒕え�씠�떎. 異� 寃쎌뿰, �띁�젅�씠�뱶, 媛곸쥌 異� ���쉶 �벑 �떎�뼇�븳 蹂쇨굅由ъ� 利먭만 嫄곕━濡� 媛��뱷�븯�떎.","36.783766","127.168874");
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("由ш컖誘몄닠愿�", "媛��쓣"," 異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� �깭議곗궛湲� 245","議곌컖媛� �씠醫낃컖�쓣 湲곕뀗�븯�뒗 由ш컖誘몄닠愿��� 洹몄쓽 �삁�닠�꽦�쓣 �뿰援�, 湲곕뀗�븯�뒗 怨녹씠�떎. �삉�븳 �섏꽌�슱�씠 �븘�땶 吏��뿭�숈뿉�꽌�룄 �삁�닠�쓣 利먭꺼蹂댁옄�뒗 �떆�룄濡쒖뜥 媛곸쥌 臾명솕 �삁�닠 �옉�뭹�뱾�쓣 �쟾�떆�븯怨� �엳�떎. 洹몃퓧留� �븘�땲�씪 �긽 �듃�씤 怨녹뿉 �옄由� �옟怨� �엳�쑝誘�濡� 硫뗭쭊 �뭾寃쎌쓣 利먭린湲곗뿉�룄 洹몃쭔�씠�떎.","36.820734","127.186147");

insert into weather(Name, weathertype, Location, body, loc , lng)  values ("愿묐뜒�궗", "寃⑥슱","異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� 愿묐뜒硫� 愿묐뜒�궗湲� 30","愿묐뜒�궗�뒗 �떊�씪 �꽑�뜒�뿬�솗 �븣 �옄�옣�쑉�궗媛� 李쎄굔�븯怨� 吏꾩궛 ���궗媛� 以묎굔�븳 �젅�씠�떎. 寃쎄린, 異⑹껌 吏�諛⑹뿉�꽌 媛��옣 �겙 �젅�씠�뿀�쑝�굹, �엫吏꾩솢���쑝濡� 遺덉뿉 �� 洹� �씠�썑�뿉 ���썒�쟾怨� 泥쒕텋�쟾�쓣 以묎굔�빐 �쑀吏��븯怨� �엳�떎. 愿묐뜒�궗媛� �쑀紐낇븳 寃껋� 怨좊젮 留먯뿉 �쟾�옒�맂 �샇�몢 臾섎ぉ�쓣 泥섏쓬 �떖�� 怨녹씠湲� �븣臾몄씠�떎.","36.675931","127.042370");
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("愿묐뜒�궛 �꽕寃�", "寃⑥슱","異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� 愿묐뜒硫� 愿묐뜒�궗湲� 30","愿묐뜒�궛�� �넂�씠 699m濡� 泥쒖븞�뿉�꽌 媛��옣 �넂�� �궛�씠�떎. �삁遺��꽣 �궛�씠 �겕怨� �꼮�꼮�븯�뿬 �뜒�씠 �엳�뒗 �궛�씠�씪 �븯���떎. �삉�븳, �씠 遺�洹쇱뿉�꽌 �깮�궛�븳 �샇�몢媛� 猿띿쭏�씠 �뻼怨� �븣�씠 苑� 李� �쑀紐낇븯�떎. 泥쒖븞 12寃� 以� �젣7寃쎌쑝濡� 寃⑥슱�뿉 �삤瑜대㈃ �늿 �뜮�씤 紐⑥뒿�씠 �젅寃쎌씠�떎.","36.675991","127.042392");
insert into weather(Name, weathertype, Location, body, loc , lng)  values ("泥쒖븞醫낇빀�쑕�뼇愿�愿묒�(�뒪�뙆)", "寃⑥슱","異⑹껌�궓�룄 泥쒖븞�떆 �룞�궓援� �꽦�궓硫� 醫낇빀�쑕�뼇吏�濡� 200","以묐�沅� 理쒓퀬 �떆�꽕�쓣 �옄�옉�븯�뒗 泥쒖븞醫낇빀�쑕�뼇愿�愿묒��뿉�꽌�뒗 �썙�꽣�뙆�겕/�뒪�뙆, �떎�뼇�븳 而⑥뀎�쓽 �뙣諛�由� 由ъ“�듃 (��紐낅━議고듃)瑜� �씠�슜�븷 �닔 �엳�떎. 泥쒖븞12寃� 以� �젣8寃쎌뿉 �냽�븯�뒗 泥쒖븞醫낇빀�쑕�뼇愿�愿묒��뒗 �뿬由꾩뿉�뒗 �썙�꽣�뙆�겕, 寃⑥슱�뿉�뒗 �뒪�뙆瑜� �슫�쁺�븯誘�濡� �궗怨꾩젅 �궡�궡 利먭만 �닔 �엳�떎.","36.757470","127.224208");



insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng) values ("紐⑤�李�","041-558-9797", "異⑸궓 泥쒖븞�떆 �룞�궓援�  嫄곕━10湲� 17", "�븳�떇", "1留뚯썝��", "9:30" ,"36.81794319999999" ,"127.15572159999999");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng) values ("���븯�닔�떇�떦","041-553-1906", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 14", "�씪�떇", "1留뚯썝��","11:30", "36.8177656" ,"127.15532229999997");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("��諛뺤궪寃뱀궡","041-555-7792", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�9湲� 13", "怨좉린", "1留뚯썝��","11:30", "36.8176253" ,"127.15549759999999");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("留덈�二쇰갑","070-4177-1116", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�9湲� 15", "�뼇�떇", "1留뚯썝��","12:00", "36.817997", "127.15549759999999" );
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("怨좏뼢�깮怨좉린","041-572-8878", "異⑸궓 泥쒖븞�떆 �룞�궓援� �닔怨� 1湲� 10", "怨좉린", "1留뚯썝��","11:00", "36.7891278", "127.127029");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("�떗�썝吏�","070-7623-6740", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 34", "怨좉린", "2留뚯썝��","11:30", "36.8177737", "127.15748770000005");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("�뿉�뒓由촜","041-523-1031", "異⑸궓 泥쒖븞�떆 �룞�궓援� 留뚮궓濡� 43", "�뼇�떇", "2留뚯썝��","11:00", "36.8196847", "127.15438990000007");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("�꽌媛��븻荑�","041-557-8412", "異⑸궓 泥쒖븞�떆 �룞�궓援� 留뚮궓濡� 58", "�뼇�떇", "2留뚯썝��","11:30", "36.8186985", "127.15877879999994");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("怨좎뭏","041-522-5088", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 19", "�씪�떇", "1留뚯썝��","11:30", "36.8179459", "127.15584710000007");
insert into restaurant(Name, RNumber, Location, Type, Price, OpenTime, loc , lng)  values ("sugo","041-522-5088", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 19", "�씪�떇", "1留뚯썝��","11:30", "36.8179459", "127.15584710000007");

insert into blogpost(blogname, blogtitle, blogdata, blogbody, link, count, fid) values ("�떖�떖���씠 釉붾줈洹�", "[泥쒖븞 留쏆쭛] �떗媛덈퉬 �젣�옉�냼 ", "2018-01-16"," �떊遺��룞 �빞�슦由� 臾댄븳由ы븘", "https://blog.naver.com/leedh428/221478789298",1, 10);
insert into blogpost(blogname, blogtitle, blogdata, blogbody, link, count, fid) values ("�씗�옱 釉붾줈洹�", "[泥쒖븞 留쏆쭛] �궪寃뱀궡 �젣�옉�냼 ", "2018-07-05"," �떊遺��룞  臾댄븳由ы븘", "https://blog.naver.com/leedh428/221531386838",1,1);
insert into blogpost(blogname, blogtitle, blogdata, blogbody, link, count, fid) values ("誘쇱젙 釉붾줈洹�", "[泥쒖븞 留쏆쭛] �냼二� �젣�옉�냼 ", "2018-04-10"," �떊遺��룞 �빞�슦由� 臾댄븳由ы븘", "https://blog.naver.com/leedh428/221478789298",1,5);



insert into travel(name, location, loc, lng, fid) values ("紐⑤�李�","異⑸궓 泥쒖븞�떆 �룞�궓援�  嫄곕━10湲� 17", "36.81794319999999" ,"127.15572159999999", 1);
insert into travel(name, location, loc, lng, fid) values ("���븯�닔�떇�떦", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 14",  "36.8177656" ,"127.15532229999997", 2);
insert into travel(name, location, loc, lng, fid)   values ("��諛뺤궪寃뱀궡", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�9湲� 13","36.8176253" ,"127.15549759999999", 3);
insert into travel(name, location, loc, lng, fid)  values ("留덈�二쇰갑", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�9湲� 15","36.817997", "127.15549759999999", 4 );
insert into travel(name, location, loc, lng, fid)  values ("怨좏뼢�깮怨좉린", "異⑸궓 泥쒖븞�떆 �룞�궓援� �닔怨� 1湲� 10","36.7891278", "127.127029", 5);
insert into travel(name, location, loc, lng, fid)  values ("�떗�썝吏�", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 34",  "36.8177737", "127.15748770000005", 6);
insert into travel(name, location, loc, lng, fid)   values ("�뿉�뒓由촜", "異⑸궓 泥쒖븞�떆 �룞�궓援� 留뚮궓濡� 43","36.8196847", "127.15438990000007", 7);
insert into travel(name, location, loc, lng, fid)   values ("�꽌媛��븻荑�", "異⑸궓 泥쒖븞�떆 �룞�궓援� 留뚮궓濡� 58", "36.8186985", "127.15877879999994", 8);
insert into travel(name, location, loc, lng, fid)  values ("怨좎뭏","異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 19", "36.8179459", "127.15584710000007", 9);
insert into travel(name, location, loc, lng, fid)  values ("sugo", "異⑸궓 泥쒖븞�떆 �룞�궓援� 癒밴굅由�10湲� 19", "36.8179459",  "127.15584710000007", 10);


-- auto_increment �엳�뒗 寃쎌슦�뿉 �궘�젣 �떆 
/* 
	alter table (�뀒�씠釉� 紐�) auto_increment=1;
	set @count = 0;
    update (�뀒�씠釉� 紐�) set (�빐�떦 auto_increment �뻾) = @count:=@count+1;
*/

