package service.user;

import java.util.List;

import javax.servlet.http.HttpSession;
import domain.user.User;

public interface UserService {

	User login(User user);
	User read(String id);
	public List<User> getUserList();
	public User getUserInfo(String id);
	public User insertUser(User user); 
	public void updateUser(User user);
	public int deleteUser(String id);
	User updateUserId(String id);
	public void logout(HttpSession session);
	public User idCheck(String id);
	public User master(String master);
}