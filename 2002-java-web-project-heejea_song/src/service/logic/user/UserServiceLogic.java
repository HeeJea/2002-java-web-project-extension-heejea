package service.logic.user;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.user.User;
import service.user.UserService;
import store.User.UserStore;

@Service
public class UserServiceLogic implements UserService {
	@Autowired
	private UserStore store;
	
	public User login(User user) {
		
		User users = null;
		if (validate(user)) {
			users = store.read(user.getId());
		}
		return users;
	}

	@Override
	public User read(String id) {
		
		return store.read(id);
	}
	@Override
	public User master(String master) {
		
		return store.master(master);
	}

	@Override
	public List<User> getUserList() {
		return store.getUserList();
	}

	@Override
	public User getUserInfo(String id) {
		return store.getUserInfo(id);
	}

	@Override
	public User insertUser(User user){

		
//		if(validate_create(user) == true)
//		{
			return store.insertUser(user);	
//		}	
//		return user;
	}

	@Override
	public void updateUser(User user) {
		System.out.println("수정 시작");
		 store.updateUser(user);
	}

	@Override
	public int deleteUser(String id) {
		return store.deleteUser(id);
		
		
	}
	
	private boolean validate(User user) {

		if (user == null) {
			throw new RuntimeException("사용자 정보가 없습니다.");
		} else if (user.getId() == null || user.getId().isEmpty()) {
			throw new RuntimeException("ID가 없습니다.");
		} else if (user.getPassword() == null || user.getPassword().isEmpty()) {
			throw new RuntimeException("비밀번호가 없습니다.");
		}
		return true;
	}

	@Override
	public User updateUserId(String id) {
		return store.updatereadUserId(id);
	}

	@Override
	public void logout(HttpSession session) {
		session.invalidate();
		
	}
	@Override
	public User idCheck(String id) {
		
		return store.idCheck(id);	
	}


}
