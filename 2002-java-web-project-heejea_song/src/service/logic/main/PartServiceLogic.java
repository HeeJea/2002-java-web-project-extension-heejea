package service.logic.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.main.Part;
import service.main.PartService;
import store.main.PartStore;

@Service
public class PartServiceLogic implements PartService{

	@Autowired
	private PartStore store;
	
	@Override
	public List<Part> partall() {
		return store.readpartall();
	}

	@Override
	public Part programeselect(String tvprogram) {
		return store.readprogrameselect(tvprogram);
	}

}
