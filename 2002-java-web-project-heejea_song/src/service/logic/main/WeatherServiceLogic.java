package service.logic.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.main.Restaurant;
import domain.main.Weather;
import service.main.WeatherService;
import store.main.WeatherStore;

@Service
public class WeatherServiceLogic implements WeatherService{

	@Autowired
	private WeatherStore store;
	
	@Override
	public List<Restaurant> AllRestaurant() {
		return store.readAllRestaurant();
	}

	@Override
	public List<Weather> TypeClassify() {
		// TODO Auto-generated method stub
		return store.readTypeClassify();
	}

	@Override
	public List<Weather> spring() {
		// TODO Auto-generated method stub
		return store.readspring();
	}

	@Override
	public List<Weather> summer() {
		// TODO Auto-generated method stub
		return store.readsummer();
	}

	@Override
	public List<Weather> autumn() {
		// TODO Auto-generated method stub
		return store.readautumn();
	}

	@Override
	public List<Weather> winter() {
		// TODO Auto-generated method stub
		return store.readwinter();
	}

	@Override
	public Weather weathercheck(String weathertype) {
		// TODO Auto-generated method stub
		return store.readweathercheck(weathertype);
	}

}
