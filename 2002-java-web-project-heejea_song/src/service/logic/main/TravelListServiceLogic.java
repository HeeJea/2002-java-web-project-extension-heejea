package service.logic.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.main.TravelList;
import service.main.TravelListService;
import store.main.TravelListStore;

@Service
public class TravelListServiceLogic implements  TravelListService {

	@Autowired
	private TravelListStore store;
	
	
	@Override
	public List<TravelList> AllList() {
		// TODO Auto-generated method stub
		return store.readAllList();
	}


	@Override
	public List<TravelList> TypeList(String name) {
		// TODO Auto-generated method stub
		return store.readtypelist(name);
	}


	@Override
	public TravelList Traveltype(String name) {
		// TODO Auto-generated method stub
		return store.readtraveltype(name);
	}


	@Override
	public TravelList Travelcenter(String name) {
		// TODO Auto-generated method stub
		return store.readTravelcenter(name);
	}

}
