package service.logic.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.main.Travel;
import domain.main.TravelList;
import service.main.TravelService;
import store.main.TravelStore;

@Service
public class TravelServiceLogic implements TravelService {

	@Autowired
	private TravelStore store;
	
	@Override
	public List<Travel> TravelSeachByAll() {
		return store.TravelreadByAll();
	}

	@Override
	public List<Travel> TravelSerachByName(String name) {
		return store.TravelreadByName(name);
	}


}
