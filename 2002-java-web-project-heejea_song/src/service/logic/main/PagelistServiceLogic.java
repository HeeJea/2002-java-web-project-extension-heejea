package service.logic.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.main.Pagelist;
import domain.main.Restaurant;
import service.main.PagelistService;
import store.main.PagelistStore;

@Service
public class PagelistServiceLogic implements PagelistService {

	@Autowired
	private PagelistStore store;
	
	@Override
	public List<Pagelist> pageall() {
		// TODO Auto-generated method stub
		return store.readpageall();
	}

	@Override
	public Pagelist pageappend(int pageid) {
		// TODO Auto-generated method stub
		return store.readpageappend(pageid);
	}

	@Override
	public Pagelist pagesearchid(int pageid) {
		// TODO Auto-generated method stub
		return store.readpagesearchid(pageid);
	}

	@Override
	public List<Restaurant> pageidlist(int count, int fin) {
		// TODO Auto-generated method stub
		return store.readpageidlist(count, fin);
	}

}
