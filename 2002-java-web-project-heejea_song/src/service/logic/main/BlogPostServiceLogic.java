package service.logic.main;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.main.BlogPost;
import service.main.BlogPostService;
import store.main.BlogPostStore;

@Service
public class BlogPostServiceLogic implements BlogPostService{
	@Autowired
	private BlogPostStore store;
	
	@Override
	public List<BlogPost> searchByBlogAll() {
		return store.readByAllBlog();
	}

//	@Override
//	public List<BlogPost> serachByBlogfid(Integer foodId) {
//		return store.readByBlogfid(foodId);
//	}

	@Override
	public BlogPost blogCounting(String blogname) {
		// TODO Auto-generated method stub
		return store.readcounting(blogname);
	}

	@Override
	public BlogPost blogdetail(String blogname) {
		// TODO Auto-generated method stub
		return store.readblogdetail(blogname);
	}

	@Override
	public int blognumber(int blogcount) {
		return store.readblognumber(blogcount);
	}

	@Override
	public BlogPost blogup(int blogid) {
		// TODO Auto-generated method stub
		return store.readblogup(blogid);
	}

	@Override
	public BlogPost blogdown(int blogid) {
		// TODO Auto-generated method stub
		return store.readblogdown(blogid);
	}

	@Override
	public BlogPost blogreplace(int blogid) {
		return store.readblogreplace(blogid);
	}

	@Override
	public List<BlogPost> searchdesc() {
		// TODO Auto-generated method stub
		return store.readsearchdesc();
	}

}
