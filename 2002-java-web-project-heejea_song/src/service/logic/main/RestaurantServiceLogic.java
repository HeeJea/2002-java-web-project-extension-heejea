package service.logic.main;

import java.io.File;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import domain.main.BlogPost;
import domain.main.Restaurant;
import domain.notice.NoticePost;
import service.main.RestaurantService;
import store.main.RestaurantStore;

@Service
public class RestaurantServiceLogic implements RestaurantService {

	@Autowired
	private RestaurantStore store;

	@Resource(name = "uploadPath")
	private String uploadPath;

	
	@Override
	public List<Restaurant> SearchTypeRestaurant(String type) {
		return store.readByType(type);
	}

	@Override
	public List<Restaurant> SearchByName(String name) {
		// TODO Auto-generated method stub
		return store.readByName(name);
	}

	@Override
	public List<Restaurant> searchAll() {
		return store.readAll();
	}

	@Override
	public List<BlogPost> findBlog(Integer foodId) {
		return store.readBlog(foodId);
	}

	@Override
	public Restaurant detailByName(Integer foodId) {
		return store.readDetail(foodId);
	}

	@Override
	public Restaurant SearchFoodIdRestaurant(String name) {
		// TODO Auto-generated method stub
		return store.readFoodIdRestaurant(name);
	}

	@Override
	public void addPhoto(Restaurant restaurant) throws Exception {
		MultipartFile[] files = restaurant.getFiles();

		if (files == null) {
			return;
		}

		for (MultipartFile fileName : files) {
			
			String savedName = uploadFile(fileName.getOriginalFilename(), fileName.getBytes());

			store.setAllPhoto(savedName, restaurant);
		}
	}

	private String uploadFile(String originalName, byte[] fileData) throws Exception {
		UUID uid = UUID.randomUUID();

		String savedName = uid.toString() + "_" + originalName;

		File target = new File(uploadPath + "\\restaurant", savedName);

		FileCopyUtils.copy(fileData, target);

		return savedName;
	}
	

	@Override
	public InputStream findPhotoById(Integer foodId, Integer photoId) {
		return store.getPhotoById(foodId, photoId);
	}

	@Override
	public List<Restaurant> findPhotoId(Integer foodId) {
		// TODO Auto-generated method stub
		return store.getPhotoId(foodId);
	}

	@Override
	public void updateSoar(Integer foodId) {
		// TODO Auto-generated method stub
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		store.updateSoarbyDate(foodId, today);
	}

	@Override
	public List<Restaurant> findSoarPhotoId() {
		// TODO Auto-generated method stub
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		return store.getSoarPhotoId(today);
	}

	@Override
	public InputStream findSoarPhotoById(int foodId) {
		// TODO Auto-generated method stub
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		return store.getSoarPhotoById(foodId, today);
	}

	@Override
	public List<Restaurant> sinbo() {
		// TODO Auto-generated method stub
		return store.readsinbo();
	}

	@Override
	public List<Restaurant> dojung() {
		// TODO Auto-generated method stub
		return store.readdojung();
	}

	@Override
	public List<Restaurant> bungchen() {
		// TODO Auto-generated method stub
		return store.readbungchen();
	}

	@Override
	public List<Restaurant> beakseok() {
		// TODO Auto-generated method stub
		return store.readbeakseok();
	}

	@Override
	public List<Restaurant> sangmyung() {
		// TODO Auto-generated method stub
		return store.readsangmyung();
	}

	@Override
	public List<Restaurant> hoseo() {
		// TODO Auto-generated method stub
		return store.readhoseo();
	}

	@Override
	public List<Restaurant> dankook() {
		// TODO Auto-generated method stub
		return store.readdankook();
	}

	@Override
	public List<Restaurant> Searchweather(String weathertype) {
		// TODO Auto-generated method stub
		return store.readSearchweather(weathertype);
	}

	@Override
	public InputStream findListPhotoById(int foodId) {
		// TODO Auto-generated method stub
		return store.getListPhotoById(foodId);
	}

	@Override
	public List<NoticePost> findSoarArticles() {
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		return store.getSoarArticles(today);
	}

}
