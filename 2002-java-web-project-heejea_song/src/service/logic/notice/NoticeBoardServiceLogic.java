package service.logic.notice;

import java.io.File;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import domain.notice.Comment;
import domain.notice.NoticePost;
import service.notice.NoticeBoardService;
import store.notice.CommentStore;
import store.notice.NoticePostStore;

@Service
public class NoticeBoardServiceLogic implements NoticeBoardService {

	@Autowired
	private NoticePostStore store;

	@Autowired
	private CommentStore comstore;

	@Resource(name = "uploadPath")

	private String uploadPath;

	@Override
	public void RegisterPost(NoticePost noticepost) throws Exception {
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		noticepost.setRegDate(today);

		store.PostCreate(noticepost);

		MultipartFile[] files = noticepost.getFiles();

		if (files == null) {
			return;
		}

		for (MultipartFile fileName : files) {
			String savedName = uploadFile(fileName.getOriginalFilename(), fileName.getBytes());

			store.retrieveAllPhoto(savedName);
		}
	}

	private String uploadFile(String originalName, byte[] fileData) throws Exception {
		UUID uid = UUID.randomUUID();

		String savedName = uid.toString() + "_" + originalName;

		File target = new File(uploadPath, savedName);

		FileCopyUtils.copy(fileData, target);

		return savedName;
	}

	@Override
	public NoticePost FindBoardPost(int bno) {

		return store.Postretrieve(bno);
	}

	@Override
	public void ModifyPost(NoticePost noticepost) throws Exception {

		store.PostUpdate(noticepost);

		MultipartFile[] files = noticepost.getFiles();
		String[] files2 = noticepost.getFiles2();
		Integer[] photoIds = noticepost.getPhotoIds();

		System.out.println(files);
		System.out.println(files2);
		System.out.println(photoIds);

		if (photoIds == null && files == null && files2 == null) {
			return;
		} else if (photoIds == null && files != null && files2 == null) {
			for (MultipartFile fileName : files) {
				String savedName = uploadFile(fileName.getOriginalFilename(), fileName.getBytes());

				store.modifyAllPhoto(noticepost, savedName);
			}
		} else if (photoIds == null && files == null && files2 != null) {
			for (String fileName : files2) {
				String savedName = fileName;

				store.modifyAllPhoto(noticepost, savedName);
			}
		} else if (photoIds != null && files != null && files2 != null) {
			for (Integer photoId : photoIds) {
				store.deletePhoto(photoId);
			}
			for (String fileName : files2) {
				String savedName = fileName;

				store.modifyAllPhoto(noticepost, savedName);
			}
			for (MultipartFile fileName : files) {
				String savedName = uploadFile(fileName.getOriginalFilename(), fileName.getBytes());

				store.modifyAllPhoto(noticepost, savedName);
			}
		} else if (photoIds != null && files != null && files2 == null) {
			for (Integer photoId : photoIds) {
				store.deletePhoto(photoId);
			}

			for (MultipartFile fileName : files) {
				String savedName = uploadFile(fileName.getOriginalFilename(), fileName.getBytes());

				store.modifyAllPhoto(noticepost, savedName);
			}
		} else if (photoIds != null && files == null && files2 != null) {
			for (Integer photoId : photoIds) {
				store.deletePhoto(photoId);
			}
			for (String fileName : files2) {
				String savedName = fileName;

				store.modifyAllPhoto(noticepost, savedName);
			}
		} else if (photoIds != null && files == null && files2 == null) {
			for (Integer photoId : photoIds) {
				store.deletePhoto(photoId);
			}
		}
	}

	@Override
	public void RemovePost(int bno) {
		store.PostDelate(bno);

	}

	@Override
	public List<NoticePost> retrieveAll() {
		return store.PostretrieveAll();
	}

	@Override
	public void ReadCnt(int bno) {

		store.updateReadCnt(bno);
	}

	@Override
	public void createComment(Comment comment) {
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		comment.setDate(today);

		comstore.CommentCreate(comment);
	}

	@Override
	public List<Comment> CheckAll(int bno) {

		return comstore.ComCheckAll(bno);
	}

	@Override
	public Comment Check(int cno) {
		return comstore.ComCheck(cno);
	}

	@Override
	public void updateComment(Comment comment) {
		comstore.ComUpdate(comment);
	}

	@Override
	public void deleteComment(int cno) {
		comstore.ComDelete(cno);
	}

	@Override
	public List<NoticePost> findPhotoId(int bno) {
		return store.getPhotoId(bno);
	}

	@Override
	public InputStream findPhotoById(int bno, int photoId) {
		return store.getPhotoById(bno, photoId);
	}

	@Override
	public List<NoticePost> findPhotoName(int bno) {
		// TODO Auto-generated method stub
		return store.getPhotoName(bno);
	}

	@Override
	public void removePhoto(Integer[] photoIds) {
		// TODO Auto-generated method stub
		if (photoIds == null) {
			return;
		}
		for (Integer photoId : photoIds) {
			store.deletePhoto(photoId);
		}
	}
	
	@Override
	public void updateSoarArticle(Integer bno) {
		// TODO Auto-generated method stub
		Date today = new Date(Calendar.getInstance().getTimeInMillis());
		store.updateSoarArticlebyDate(bno, today);
	}

	@Override
	public NoticePost findAuthor(int bno) {
		// TODO Auto-generated method stub
		return store.findAuthorId(bno);
	}

}
