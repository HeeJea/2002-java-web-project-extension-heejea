package service.main;

import java.io.InputStream;
import java.util.List;

import domain.main.BlogPost;
import domain.main.Restaurant;
import domain.notice.NoticePost;

public interface RestaurantService {

	
	List<Restaurant> SearchTypeRestaurant(String type); 
	Restaurant SearchFoodIdRestaurant(String name);
	List<Restaurant> SearchByName(String name);
	List<Restaurant> searchAll(); 
	List<BlogPost> findBlog(Integer foodId);
	Restaurant detailByName(Integer foodId); 
	void addPhoto(Restaurant restaurant) throws Exception;
	InputStream findPhotoById(Integer foodId, Integer photoId);
	List<Restaurant> findPhotoId(Integer foodId);
	void updateSoar(Integer foodId);
	List<Restaurant> findSoarPhotoId();
	InputStream findSoarPhotoById(int foodId);
	InputStream findListPhotoById(int foodId);
	
	// travel
	List<Restaurant> sinbo();
	List<Restaurant> dojung();
	List<Restaurant> bungchen();
	List<Restaurant> beakseok();
	List<Restaurant> sangmyung();
	List<Restaurant> hoseo();
	List<Restaurant> dankook();
	
	//
	List<Restaurant> Searchweather(String weathertype);
	List<NoticePost> findSoarArticles();
	
}