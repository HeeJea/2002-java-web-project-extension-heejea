package service.main;

import java.util.List;

import domain.main.TravelList;

public interface TravelListService {

	List<TravelList> AllList();
	List<TravelList> TypeList(String name);
	TravelList Traveltype(String name);
	TravelList Travelcenter(String name);
}
