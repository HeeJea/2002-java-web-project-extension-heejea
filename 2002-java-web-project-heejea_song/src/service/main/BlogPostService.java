package service.main;

import java.util.List;

import domain.main.BlogPost;

public interface BlogPostService {

	List<BlogPost> searchByBlogAll();
//	List<BlogPost> serachByBlogfid(Integer foodId);
	BlogPost blogCounting(String blogname);
	BlogPost blogdetail(String blogname);
	public int blognumber(int count);
	BlogPost blogup(int blogid);
	BlogPost blogdown(int blogid);
	BlogPost blogreplace(int blogid);
	List<BlogPost> searchdesc();
}
