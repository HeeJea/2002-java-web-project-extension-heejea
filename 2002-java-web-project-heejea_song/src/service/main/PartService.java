package service.main;

import java.util.List;

import domain.main.Part;

public interface PartService {

	List<Part> partall();
	Part programeselect(String tvprogram);
}
