package service.main;

import java.util.List;

import domain.main.Travel;
import domain.main.TravelList;

public interface TravelService {

	List<Travel> TravelSeachByAll();
	List<Travel> TravelSerachByName(String name);
	
}
