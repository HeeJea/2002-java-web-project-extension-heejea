package service.main;

import java.util.List;

import domain.main.Restaurant;
import domain.main.Weather;

public interface WeatherService {

	List<Restaurant> AllRestaurant();
	List<Weather> TypeClassify();
	List<Weather> spring();
	List<Weather> summer();
	List<Weather> autumn();
	List<Weather> winter();
	Weather weathercheck(String weathertype); 
}
