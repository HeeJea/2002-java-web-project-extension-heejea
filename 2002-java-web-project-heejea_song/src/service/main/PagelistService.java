package service.main;

import java.util.List;

import domain.main.Pagelist;
import domain.main.Restaurant;

public interface PagelistService {

	List<Pagelist> pageall();
	Pagelist pageappend(int pageid);
	Pagelist pagesearchid(int pageid);
	List<Restaurant> pageidlist(int count, int fin);
}
