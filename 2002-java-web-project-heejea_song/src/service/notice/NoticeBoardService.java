package service.notice;

import java.io.InputStream;
import java.util.List;

import domain.notice.Comment;
import domain.notice.NoticePost;

public interface NoticeBoardService  {

	void RegisterPost(NoticePost post) throws Exception; // �Խñ� ���
	NoticePost FindBoardPost(int bno); // �Խñ� ��ȸ
	void ModifyPost(NoticePost noticepost) throws Exception; // �Խñ� ����
	void RemovePost(int bno); // �Խñ� ����
	List<NoticePost> retrieveAll(); //�Խñ� ��ü ���
	void ReadCnt(int bno); //��ȸ��
	
    
	List<Comment> CheckAll(int bno);//��۸��
	Comment Check(int cno);
	void createComment(Comment comment); //����߰�
	void updateComment(Comment comment);
	void deleteComment(int cno);
	
	List<NoticePost> findPhotoId(int bno);
	InputStream findPhotoById(int bno, int photoId);
	List<NoticePost> findPhotoName(int bno);
	void removePhoto(Integer[] photoIds);
	void updateSoarArticle(Integer bno);
	
	NoticePost findAuthor(int bno);

	
	
}
