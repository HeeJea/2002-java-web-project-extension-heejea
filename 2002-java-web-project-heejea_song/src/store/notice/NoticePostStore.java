package store.notice;

import java.io.InputStream;
import java.sql.Date;
import java.util.List;

import domain.notice.NoticePost;

public interface NoticePostStore {

	int PostCreate(NoticePost Post);//�Խñ��ۼ�
	NoticePost Postretrieve(int bno);//�Խñ���ȸ
	void PostUpdate(NoticePost Post);//�Խñۼ���
	void PostDelate(int bno);//�Խñۻ���
	List<NoticePost> PostretrieveAll();//�Խñ� ��ü���
	void updateReadCnt(int bno);//��ȸ��
	List<NoticePost> getPhotoId(int bno);
	InputStream getPhotoById(int bno, int photoId);
	void retrieveAllPhoto(String savedName);
	List<NoticePost> getPhotoName(int bno);
	void modifyAllPhoto(NoticePost noticepost, String savedName);
	void deletePhoto(int photoId);
	void updateSoarArticlebyDate(Integer bno, Date today);
	NoticePost findAuthorId(int bno);
}
