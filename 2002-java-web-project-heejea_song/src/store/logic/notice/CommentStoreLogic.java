package store.logic.notice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.notice.Comment;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.notice.CommentStore;

@Repository
public class CommentStoreLogic implements CommentStore{

	private ConnectionFactory connectionFactory;
	
	public CommentStoreLogic() { //db����
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	@Override
	public List<Comment> ComCheckAll(int bno) {
		
		String sql = "SELECT Cno, Substance, Writer,Date,bno FROM Comment_TB where bno = ? order by Cno desc";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<Comment> lists = new ArrayList<Comment>();
		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bno);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				Comment list = new Comment();
				list.setCno(rs.getInt(1));
				list.setSubstance(rs.getString(2));
				list.setWriter(rs.getString(3));
				list.setDate(rs.getDate(4));
				list.setBno(rs.getInt(5));
				lists.add(list);
			}
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}	finally {
			JdbcUtils.close(rs,pstmt,conn);
		}
		return lists;
	}
	
	
	@Override
	public Comment ComCheck(int cno) {
		String sql = "SELECT Cno, Substance, Writer, bno  FROM Comment_TB where cno = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Comment list = null;
		
		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, cno);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				list = new Comment();
				list.setCno(rs.getInt(1));
				list.setSubstance(rs.getString(2));
				list.setWriter(rs.getString(3));
				list.setBno(rs.getInt(4));
				
			}
		} catch(SQLException e) {
			throw new RuntimeException(e);
		}	finally {
			JdbcUtils.close(rs,pstmt,conn);
		}
		return list;
	}
	
	
	@Override
	public int CommentCreate(Comment comment) {
		int tempCno = 1;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		

		try {
			String sql = "SELECT Cno FROM Comment_TB ORDER BY Cno DESC";
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			if(rs.next()) {
				tempCno=rs.getInt(1)+1;
			}
			JdbcUtils.close(psmt,conn,rs);


			String sql2 = "INSERT INTO Comment_TB(Cno,Substance, Writer,Date,bno) VALUES(?,?,?,?,?)";
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql2);
			psmt.setInt(1, tempCno);
			psmt.setString(2, comment.getSubstance());
			psmt.setString(3, comment.getWriter());
			psmt.setDate(4, comment.getDate());
			psmt.setInt(5, comment.getBno());
			return psmt.executeUpdate();

		} catch(SQLException e) {
			e.printStackTrace();
		}	finally {
			JdbcUtils.close(psmt,conn,rs);
		}
		return -1; //db����

	}
	
	@Override
	public void ComUpdate(Comment comment) {
		String sql = "UPDATE Comment_TB SET Substance = ? WHERE Cno = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;


		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setString(1, comment.getSubstance());
			pstmt.setInt(2, comment.getCno());
			pstmt.executeUpdate();


		} catch(SQLException e) {
			throw new RuntimeException(e);
		}	finally {
			JdbcUtils.close(pstmt,conn);
		}
	}
	
	@Override
	public void ComDelete(int cno) {
		
		System.out.print("store111");
		Connection conn = null;
		PreparedStatement pstmt = null;


		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM Comment_TB WHERE Cno=?");

			pstmt.setInt(1,cno);

			pstmt.executeUpdate();


		} catch(SQLException e) {
			throw new RuntimeException(e);
		}	finally {
			JdbcUtils.close(pstmt,conn);
		}
	}
	
	
}
