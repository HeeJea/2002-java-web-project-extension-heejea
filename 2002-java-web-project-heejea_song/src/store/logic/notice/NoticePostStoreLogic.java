package store.logic.notice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import domain.notice.NoticePost;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.notice.NoticePostStore;

@Repository
public class NoticePostStoreLogic implements NoticePostStore {

	private ConnectionFactory connectionFactory;

	@Resource(name = "uploadPath")
	private String uploadPath;

	public NoticePostStoreLogic() { // db占쏙옙占쏙옙
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public int PostCreate(NoticePost Post) {

		int tempBno = 1;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT bno FROM Notice_Post_TB ORDER BY bno DESC";
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			if (rs.next()) {
				tempBno = rs.getInt(1) + 1;
			}
			JdbcUtils.close(psmt, conn, rs);

			String sql2 = "INSERT INTO Notice_Post_TB(bno,Title, Contents, AuthorName,RegDate) VALUES(?,?,?,?,?)";
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql2);
			psmt.setInt(1, tempBno);
			psmt.setString(2, Post.getTitle());
			psmt.setString(3, Post.getContents());
			psmt.setString(4, Post.getAuthorName());
			psmt.setDate(5, Post.getRegDate());
			return psmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(psmt, conn, rs);
		}
		return -1; // db占쏙옙占쏙옙

	}

	@Override
	public NoticePost Postretrieve(int bno) {
		String sql = "SELECT bno, Title, AuthorName, RegDate, viewcnt, Contents " + "FROM Notice_Post_TB "
				+ "where bno=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		NoticePost post = null;

		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				post = new NoticePost();
				post.setBno(rs.getInt("bno"));
				post.setTitle(rs.getString("Title"));
				post.setAuthorName(rs.getString("AuthorName"));
				post.setRegDate(rs.getDate("RegDate"));
				post.setViewcnt(rs.getInt("viewcnt"));
				post.setContents(rs.getString("Contents"));
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, pstmt, conn);
		}
		return post;
	}

	@Override
	public void PostUpdate(NoticePost Post) {
		// String sql = "UPDATE Notice_Post_TB SET Contents = ? WHERE bno = ?";
		String sql = "UPDATE Notice_Post_TB SET Title = ?, Contents = ? WHERE bno = ?";
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, Post.getTitle());
			pstmt.setString(2, Post.getContents());
			pstmt.setInt(3, Post.getBno());
			pstmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(pstmt, conn);
		}

	}

	@Override
	public void PostDelate(int bno) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt2 = null;
		PreparedStatement pstmt3 = null;
		ResultSet rs = null;
		List<Integer> idTemps = new ArrayList<Integer>();
		int i;
		int temp = 0;
		int idTemp = 0;
		
		
		try {
			// �뙎湲��궘�젣
			conn = connectionFactory.createConnection();
			String sql = "DELETE FROM Comment_TB WHERE bno=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bno);
			pstmt.executeUpdate();
			JdbcUtils.close(pstmt, conn);

			// 寃뚯떆�뙋�궘�젣
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM Notice_Post_TB WHERE bno=?");
			pstmt.setInt(1, bno);
			pstmt.executeUpdate();
			JdbcUtils.close(pstmt, conn);
			
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("DELETE FROM soar_Article WHERE bno=?");
			pstmt.setInt(1, bno);
			pstmt.executeUpdate();
			JdbcUtils.close(pstmt, conn);
			
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("select photoId from Notice_photo where bno = ?");
			pstmt.setInt(1, bno);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				idTemp = rs.getInt(1);
				idTemps.add(idTemp);
			}
			for(Integer id : idTemps) {
				pstmt = conn.prepareStatement("DELETE FROM Notice_photo WHERE photoId=?");
				pstmt.setInt(1, id);
				pstmt.executeUpdate();
			}

			// 寃뚯떆湲�踰덊샇, �궗吏� 議곗젙
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("select max(last_insert_id(bno)) from Notice_Post_TB");

			rs = pstmt.executeQuery();
			while (rs.next()) {
				temp = rs.getInt(1);
			}

			for (i = bno; i < temp; i++) {
				pstmt = conn.prepareStatement("UPDATE Notice_Post_TB SET bno = bno-1 WHERE bno = ?");
				pstmt2 = conn.prepareStatement("UPDATE Notice_photo SET bno = bno-1 WHERE bno = ?");
				pstmt3 = conn.prepareStatement("UPDATE soar_Article SET bno = bno-1 WHERE bno = ?");
				pstmt.setInt(1, i + 1);
				pstmt2.setInt(1, i + 1);
				pstmt3.setInt(1, i + 1);
				pstmt.executeUpdate();
				pstmt2.executeUpdate();
				pstmt3.executeUpdate();
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(pstmt, conn);
			JdbcUtils.close(pstmt2, conn);
		}
	}

	@Override
	public List<NoticePost> PostretrieveAll() {
		String sql = "SELECT bno, Title, AuthorName, RegDate, viewcnt FROM Notice_Post_TB order by bno desc";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		List<NoticePost> posts = new ArrayList<NoticePost>();
		try {
			conn = connectionFactory.createConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {
				NoticePost post = new NoticePost();
				post.setBno(rs.getInt(1));
				post.setTitle(rs.getString("Title"));
				post.setAuthorName(rs.getString(3));
				post.setRegDate(rs.getDate(4));
				post.setViewcnt(rs.getInt(5));
				posts.add(post);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, stmt, conn);
		}
		return posts;
		// return null;
	}

	@Override
	public void updateReadCnt(int bno) {
		Connection conn = null;
		PreparedStatement psmt = null;
		try {

			String sql = "update Notice_Post_TB set viewcnt=viewcnt+1 where bno=?";

			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement(sql);
			psmt.setInt(1, bno);
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public void retrieveAllPhoto(String savedName) {
		Connection conn = null;
		PreparedStatement psmt = null;
		PreparedStatement psmt2 = null;
		ResultSet rs = null;
		int temp = 0;

		String sql = "select max(last_insert_id(bno)) from Notice_Post_TB";
		try {
			conn = connectionFactory.createConnection();
			psmt2 = conn.prepareStatement(sql.toString());
			rs = psmt2.executeQuery();
			while (rs.next()) {
				temp = rs.getInt(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt2, conn);
		}

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("INSERT INTO Noticephoto (bno, noticePhoto, noticePhotoName) VALUES (?,?,?)");

			File target = new File(uploadPath, savedName);

			FileInputStream inputStream;

			System.out.println(savedName);
			System.out.println(target);

			try {
				inputStream = new FileInputStream(target);

				psmt.setInt(1, temp);
				psmt.setBinaryStream(2, inputStream);
				psmt.setString(3, savedName);

				psmt.executeUpdate();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}

	}

	@Override
	public List<NoticePost> getPhotoId(int bno) {

		String sql = "SELECT PhotoId FROM Noticephoto WHERE bno = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<NoticePost> photoIds = new ArrayList<NoticePost>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, bno);
			rs = psmt.executeQuery();
			while (rs.next()) {
				NoticePost photoId = new NoticePost();
				photoId.setPhotoId(rs.getInt(1));
				photoIds.add(photoId);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return photoIds;
	}

	@Override
	public InputStream getPhotoById(int bno, int photoId) {
		String sql = "SELECT noticePhoto  FROM Noticephoto WHERE bno = ? and PhotoId = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		InputStream photo = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, bno);
			psmt.setInt(2, photoId);
			rs = psmt.executeQuery();
			while (rs.next()) {
				photo = rs.getBinaryStream(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return photo;
	}

	@Override
	public List<NoticePost> getPhotoName(int bno) {
		String sql = "SELECT photoId, noticePhotoName FROM Noticephoto WHERE bno = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<NoticePost> photoId = new ArrayList<NoticePost>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, bno);
			rs = psmt.executeQuery();
			while (rs.next()) {
				NoticePost photoName = new NoticePost();
				photoName.setPhotoId(rs.getInt(1));
				photoName.setPhotoName(rs.getString(2));
				photoId.add(photoName);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return photoId;
	}

	@Override
	public void modifyAllPhoto(NoticePost noticepost, String savedName) {
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("INSERT INTO Noticephoto (bno, noticePhoto, noticePhotoName) VALUES (?,?,?)");

			File target = new File(uploadPath, savedName);

			FileInputStream inputStream;

			System.out.println(savedName);
			System.out.println(target);

			try {
				inputStream = new FileInputStream(target);

				psmt.setInt(1, noticepost.getBno());
				psmt.setBinaryStream(2, inputStream);
				psmt.setString(3, savedName);

				psmt.executeUpdate();

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}

	}

	@Override
	public void deletePhoto(int photoId) {
		Connection conn = null;
		PreparedStatement psmt = null;

		String sql = "DELETE FROM Noticephoto WHERE photoId = ?";

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, photoId);
			psmt.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}
	
	@Override
	public void updateSoarArticlebyDate(Integer bno, Date today) {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement psmt = null;

		ResultSet rs = null;
		Date dateTemp = null;

		String sql = "SELECT date FROM soar_Article WHERE bno = ? and date = ?";
		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, bno);
			psmt.setDate(2, today);
			rs = psmt.executeQuery();
			while (rs.next()) {
				dateTemp = rs.getDate(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement("UPDATE soar_Article SET count = count + 1 WHERE bno = ? and date = ?");
			psmt.setInt(1, bno);
			psmt.setDate(2, today);
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		
		if (dateTemp == null) {
			try {
				conn = connectionFactory.createConnection();

				psmt = conn.prepareStatement("INSERT INTO soar_Article (bno, date, count) VALUES (?,?,?)");
				psmt.setInt(1, bno);
				psmt.setDate(2, today);
				psmt.setInt(3, 1);
				psmt.executeUpdate();

			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				JdbcUtils.close(psmt, conn);
			}
		}
	}

	@Override
	public NoticePost findAuthorId(int bno) {
		String sql = "SELECT AuthorName FROM Notice_Post_TB where bno=?";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		NoticePost author = null;

		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, bno);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				author = new NoticePost();
				author.setAuthorName(rs.getString("AuthorName"));
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, pstmt, conn);
		}
		return author;
		
	}
}
