package store.logic.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Repository;

import domain.user.User;
import store.User.UserStore;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;


@Repository
public class UserStoreLogic implements UserStore{

	private ConnectionFactory connectionFactory;
	
	
	public UserStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public User read(String id) {
		
		String sql = "SELECT id, password, name, email, master, area FROM User WHERE id = ?" ;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		User user = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId(rs.getString("id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setMaster(rs.getString("master"));
				user.setArea(rs.getString("area"));
			
			}
			
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return user;
}
	
	@Override
	public List<User> getUserList() {
		String sql = "SELECT id, name ,email, area from USER";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<User> users = new ArrayList<User>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery(sql);
			while(rs.next()) {
				User user = new User();
				user.setId(rs.getString("id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setArea(rs.getString("area"));
				users.add(user);
			}
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return users;
	}
	@Override
	public User master(String master) {
		String sql = "SELECT id, password, name, email, area, master FROM USER WHERE master = ?";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		User user = null;
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, master);
			rs = psmt.executeQuery(sql);
			if(rs.next())
			{
				user = new User();
				user.setId(rs.getString("id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setArea(rs.getString("area"));
				user.setMaster(rs.getString("master"));
			}
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return user;
	}
		

	@Override
	public User insertUser(User user) {
		String sql = "insert into USER(id, password, name ,email, area) VALUES (?, ?, ?, ?, ?)";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
		
			psmt.setString(1, user.getId());
			psmt.setString(2, user.getPassword());
			psmt.setString(3, user.getName());
			psmt.setString(4, user.getEmail());
			psmt.setString(5, user.getArea());
			psmt.executeUpdate();
			
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return user;
	}

	@Override
	public int readid(String id) {
		String sql = "select id from USER where id = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			rs = psmt.executeQuery(sql);
			System.out.println(id);
			if(rs.next())
			{
				count = count + 1;
			}

		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return count;
	}
	
	@Override
	public User getUserInfo(String id) {
		return null;
	}
	@Override
	public void updateUser(User user) {
		Connection conn = null;
		PreparedStatement psmt = null;
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("UPDATE USER SET password = ?, name = ?, email = ?, area = ?  where id = ?");			
			
			psmt.setString(1, user.getPassword());
			psmt.setString(2, user.getName());
			psmt.setString(3, user.getEmail());
			psmt.setString(4, user.getArea());
			psmt.setString(5, user.getId());
			
			System.out.println(user.getEmail());
			System.out.println(user.getId());
			
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		
	}
	@Override
	public int deleteUser(String id) {
		System.out.println(id);
		
		String sql = "DELETE FROM USER where id=?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		int result = 0;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id.trim()); //怨듬갚?占쏙옙占�? 硫붿냼?占쏙옙 trim()
			psmt.executeUpdate();
					}
		catch(SQLException e) {			
		throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return result;
	
	}

	@Override
	public User updatereadUserId(String id) {
	    String sql = "SELECT id, password, name, email, area FROM User WHERE id = ?" ;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		User user = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setId(rs.getString("id"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setEmail(rs.getString("email"));
				user.setArea(rs.getString("area"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return user;
	}

	@Override
	public void logout(HttpSession session) {
		}
		@Override
	public User idCheck(String id) {
		String sql = "select id from USER where id = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		User user = null;
		
	
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			
			System.out.println(id);
			
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs,psmt, conn);
		}
		return user;
		
	}
}
