package store.logic.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.main.BlogPost;
import domain.main.Pagelist;
import domain.main.Restaurant;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.PagelistStore;

@Repository
public class PagelistStoreLogic implements PagelistStore{


	private ConnectionFactory connectionFactory;
	
	public PagelistStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	@Override
	public List<Pagelist> readpageall() {
		
		String sql = "select pageid, count from page_post";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Pagelist> pagelists = new ArrayList<Pagelist>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				Pagelist pagelist = new Pagelist();
				pagelist.setPageid(rs.getInt("pageid"));
				pagelist.setCount(rs.getInt("count"));
				pagelists.add(pagelist);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return pagelists;
	}

	@Override
	public Pagelist readpageappend(int pageid) {

		String sql = "select pageid, count from page_post where pageid = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Pagelist pagelist = null;
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, pageid);
			rs = psmt.executeQuery();
			rs.last(); 
			int rowCnt = rs.getRow(); 
			rs.first(); 
			System.out.println(rowCnt);
			if(rowCnt == 0)
			{
				JdbcUtils.close(rs, psmt, conn);
				sql = "insert into page_post(count) values(count * ?)";
				conn = null;
				psmt = null;
				rs = null;
				conn = connectionFactory.createConnection();
				psmt = conn.prepareStatement(sql.toString());
				psmt.setInt(1, pageid);
				psmt.executeUpdate();
			}
			
			}catch(SQLException e) {
				throw new RuntimeException(e);
			} finally {
				JdbcUtils.close(rs, psmt, conn);
			}
			return pagelist;
		}

	@Override
	public Pagelist readpagesearchid(int pageid) {
		
		String sql = "select pageid, count from page_post where pageid = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Pagelist pagelist = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, pageid);
			rs = psmt.executeQuery();
			if (rs.next()) {
				pagelist = new Pagelist();
				pagelist.setPageid(rs.getInt("pageid"));
				pagelist.setCount(rs.getInt("count"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return pagelist;
	}

	@Override
	public List<Restaurant> readpageidlist(int count, int fin) {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng FROM restaurant where FoodId >= ? && FoodId <= ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, (count + 1));
			psmt.setInt(2, fin);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;
	}
}

