package store.logic.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.main.BlogPost;
import domain.main.Restaurant;
import domain.main.Weather;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.WeatherStore;

@Repository
public class WeatherStoreLogic implements WeatherStore {

	private ConnectionFactory connectionFactory;
	
	public WeatherStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	
	@Override
	public List<Restaurant> readAllRestaurant() {
		
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng FROM restaurant";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return restaurants;

	}

	@Override
	public List<Weather> readTypeClassify() {
	
		String sql = "select distinct name, location, weathertype from weather";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Weather> weathers = new ArrayList<Weather>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Weather weather = new Weather();
				weather.setName(rs.getString("name"));
				weather.setLocation(rs.getString("location"));
				weather.setWeathertype(rs.getString("weathertype"));		
				weathers.add(weather);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return weathers;
	}


	@Override
	public List<Weather> readspring() {
		
		String sql = "select weatherid, name, weathertype, location, body, loc, lng from weather where weathertype= '봄'";
		
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Weather> weathers = new ArrayList<Weather>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Weather weather = new Weather();
				weather.setName(rs.getString("weatherid"));
				weather.setName(rs.getString("name"));
				weather.setWeathertype(rs.getString("weathertype"));
				weather.setLocation(rs.getString("location"));
				weather.setBody(rs.getString("body"));
				weather.setLatitude(rs.getString("loc"));
				weather.setLongitude(rs.getString("lng"));
				weathers.add(weather);
				
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return weathers;
	}


	@Override
	public List<Weather> readsummer() {
		String sql = "select weatherid, name, weathertype, location, body, loc, lng from weather where weathertype = '여름'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Weather> weathers = new ArrayList<Weather>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Weather weather = new Weather();
				weather.setName(rs.getString("weatherid"));
				weather.setName(rs.getString("name"));
				weather.setWeathertype(rs.getString("weathertype"));		
				weather.setLocation(rs.getString("location"));
				weather.setBody(rs.getString("body"));
				weather.setLatitude(rs.getString("loc"));
				weather.setLongitude(rs.getString("lng"));
				weathers.add(weather);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return weathers;
	}
	


	@Override
	public List<Weather> readautumn() {
		String sql = "select weatherid, name, weathertype, location, body, loc, lng from weather where weathertype = '가을'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Weather> weathers = new ArrayList<Weather>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Weather weather = new Weather();
				weather.setName(rs.getString("weatherid"));
				weather.setName(rs.getString("name"));
				weather.setWeathertype(rs.getString("weathertype"));		
				weather.setLocation(rs.getString("location"));
				weather.setBody(rs.getString("body"));
				weather.setLatitude(rs.getString("loc"));
				weather.setLongitude(rs.getString("lng"));
				weathers.add(weather);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return weathers;
	}
	


	@Override
	public List<Weather> readwinter() {
		String sql = "select weatherid, name, weathertype, location, body, loc, lng from weather where weathertype = '겨울'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Weather> weathers = new ArrayList<Weather>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Weather weather = new Weather();
				weather.setName(rs.getString("weatherid"));
				weather.setName(rs.getString("name"));
				weather.setWeathertype(rs.getString("weathertype"));		
				weather.setLocation(rs.getString("location"));
				weather.setBody(rs.getString("body"));
				weather.setLatitude(rs.getString("loc"));
				weather.setLongitude(rs.getString("lng"));
				weathers.add(weather);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return weathers;
	}


	@Override
	public Weather readweathercheck(String weathertype) {
		String sql = "select weatherid, name, weathertype, location, body, loc, lng from weather where weathertype = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Weather weather = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, weathertype);
			rs = psmt.executeQuery();
			while (rs.next()) {
				weather = new Weather();
				weather.setName(rs.getString("weatherid"));
				weather.setName(rs.getString("name"));
				weather.setWeathertype(rs.getString("weathertype"));		
				weather.setLocation(rs.getString("location"));
				weather.setBody(rs.getString("body"));
				weather.setLatitude(rs.getString("loc"));
				weather.setLongitude(rs.getString("lng"));
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				JdbcUtils.close(rs, psmt, conn);
			}
			return weather;
		}
}
