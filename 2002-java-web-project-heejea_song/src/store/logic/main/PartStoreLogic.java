package store.logic.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.main.Part;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.PartStore;

@Repository
public class PartStoreLogic implements PartStore {

	private ConnectionFactory connectionFactory;
	
	public PartStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	@Override
	public List<Part> readpartall() {
		String sql = "SELECT id, restaurantname, tvProgram, Date, mainmenu FROM part_tv";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Part> parts = new ArrayList<Part>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				Part part = new Part();
				part.setPartid(rs.getInt("id"));
				part.setRestaurantname(rs.getString("restaurantname"));
				part.setTvprograme(rs.getString("tvProgram"));
				part.setTvdate(rs.getString("Date"));
				part.setMainmenu(rs.getString("mainmenu"));
				parts.add(part);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return parts;
	}

	@Override
	public Part readprogrameselect(String tvprogram) {
		String sql = "SELECT id, restaurantname, tvProgram, Date, mainmenu FROM parttv where tvProgram = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Part part = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, tvprogram);
			rs = psmt.executeQuery();
			if (rs.next()) {
				part = new Part();
				part.setPartid(rs.getInt("id"));
				part.setRestaurantname(rs.getString("restaurantname"));
				part.setTvprograme(rs.getString("tvProgram"));
				part.setTvdate(rs.getString("Date"));
				part.setMainmenu(rs.getString("mainmenu"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return part;
	}

}