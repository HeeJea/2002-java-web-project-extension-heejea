package store.logic.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import domain.main.BlogPost;
import domain.main.Restaurant;
import domain.notice.NoticePost;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.RestaurantStore;
import javax.annotation.Resource;

@Repository
public class RestaurantStoreLogic implements RestaurantStore {

	private ConnectionFactory connectionFactory;

	public RestaurantStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Resource(name = "uploadPath")
	private String uploadPath;

	@Override
	public List<Restaurant> readByType(String Type) {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng FROM restaurant WHERE Type LIKE ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, Type);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;
	}

	@Override
	public List<Restaurant> readByName(String name) {

		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng  FROM restaurant WHERE Name LIKE ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, "%" + name + "%");
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;
	}

	@Override
	public List<Restaurant> readAll() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng FROM restaurant";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return restaurants;
	}

	@Override
	public Restaurant readDetail(Integer foodId) {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng  FROM restaurant WHERE foodId = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Restaurant restaurant = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, foodId);
			rs = psmt.executeQuery();
			if (rs.next()) {
				restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurant;
	}

	@Override
	public Restaurant readFoodIdRestaurant(String name) {
		String sql = "SELECT FoodId FROM restaurant WHERE name = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Restaurant restaurant = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, name);
			rs = psmt.executeQuery();

			if (rs.next()) {
				restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurant;
	}

	@Override
	public List<BlogPost> readBlog(Integer foodId) {

		String sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post where fid = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<BlogPost> blogPost = new ArrayList<BlogPost>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, foodId);
			rs = psmt.executeQuery();

			while (rs.next()) {
				BlogPost blog = new BlogPost();
				blog.setblogid(rs.getInt("blogid"));
				blog.setblogname(rs.getString("blogname"));
				blog.setblogtitle(rs.getString("blogtitle"));
				blog.setblogdata(rs.getString("blogdata"));
				blog.setblogbody(rs.getString("blogbody"));
				blog.setbloglink(rs.getString("link"));
				blog.setblogcount(rs.getInt("count"));
				blog.setfid(rs.getInt("fid"));
				blogPost.add(blog);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return blogPost;
	}

	@Override
	public void setAllPhoto(String savedName, Restaurant restaurant) {

		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement psmt = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement("INSERT INTO Restaurant_Photo (FoodId, ResPhoto) VALUES (?, ?)");
			File target = new File(uploadPath+"\\restaurant", savedName);
				
			FileInputStream inputStream;

				System.out.println(savedName);
				System.out.println(target);
				try {
					inputStream = new FileInputStream(target);
					
					psmt.setInt(1, restaurant.getFoodId());
					psmt.setBinaryStream(2, inputStream);

					psmt.executeUpdate();
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
	}

	@Override
	public InputStream getPhotoById(Integer foodId, Integer photoId) {

//		String sql = "SELECT ResPhoto FROM RestaurantPhoto WHERE FoodId = ? and PhotoId = ?";
		String sql = "SELECT ResPhoto FROM restaurant_photo WHERE FoodId = ? and PhotoId = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		InputStream photo = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, foodId);
			psmt.setInt(2, photoId);
			rs = psmt.executeQuery();
			while (rs.next()) {
				photo = rs.getBinaryStream(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return photo;
	}

	@Override
	public List<Restaurant> getPhotoId(Integer foodId) {
		// TODO Auto-generated method stub
		String sql = "SELECT PhotoId FROM restaurant_photo WHERE FoodId = ?";

		//		String sql = "SELECT PhotoId FROM RestaurantPhoto WHERE FoodId = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> photoIds = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, foodId);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant photoId = new Restaurant();
				photoId.setPhotoId(rs.getInt(1));
				photoIds.add(photoId);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return photoIds;
	}

	@Override
	public void updateSoarbyDate(Integer foodId, Date today) {
		// TODO Auto-generated method stub
		Connection conn = null;
		PreparedStatement psmt = null;

		ResultSet rs = null;
		Date dateTemp = null;

		String sql = "SELECT date FROM soar WHERE foodId = ? and date = ?";
		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, foodId);
			psmt.setDate(2, today);
			rs = psmt.executeQuery();
			while (rs.next()) {
				dateTemp = rs.getDate(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		try {
			conn = connectionFactory.createConnection();

			psmt = conn.prepareStatement("UPDATE soar SET count = count + 1 WHERE foodId = ? and date = ?");
			psmt.setInt(1, foodId);
			psmt.setDate(2, today);
			psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, conn);
		}
		
		if (dateTemp == null) {
			try {
				conn = connectionFactory.createConnection();

				psmt = conn.prepareStatement("INSERT INTO soar (foodId, date, count) VALUES (?,?,?)");
				psmt.setInt(1, foodId);
				psmt.setDate(2, today);
				psmt.setInt(3, 1);
				psmt.executeUpdate();

			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				JdbcUtils.close(psmt, conn);
			}
		}
	}

	@Override
	public List<Restaurant> getSoarPhotoId(Date today) {
		// TODO Auto-generated method stub
		String sql = "SELECT soar.foodId, PhotoId From restaurant_photo inner join soar on restaurant_photo.foodId=soar.foodId where date = ? group by foodId order by count desc limit 10";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> photoIds = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setDate(1, today);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant photoId = new Restaurant();
				photoId.setFoodId(rs.getInt(1));
				photoId.setPhotoId(rs.getInt(2));
				photoIds.add(photoId);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return photoIds;
	}

	@Override
	public InputStream getSoarPhotoById(int foodId,Date today) {
		// TODO Auto-generated method stub

		String sql = "SELECT ResPhoto From restaurant_photo inner join soar on restaurant_photo.foodId=soar.foodId where date = ? and soar.foodId = ? order by count desc limit 1";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		InputStream photo = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setDate(1, today);
			psmt.setInt(2, foodId);
			rs = psmt.executeQuery();
			while (rs.next()) {
				photo = rs.getBinaryStream(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		
		System.out.print(photo);
		return photo;
	}

	@Override
	public List<Restaurant> readsinbo() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = '�źε�'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}

	@Override
	public List<Restaurant> readdojung() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = '����'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}
	
	@Override
	public List<Restaurant> readbungchen() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = '��õ'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}

	@Override
	public List<Restaurant> readbeakseok() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = '�鼮��'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}

	@Override
	public List<Restaurant> readsangmyung() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = '����'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}

	@Override
	public List<Restaurant> readhoseo() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = 'ȣ����'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}

	@Override
	public List<Restaurant> readdankook() {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE hotplace = '�ܱ���'";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;

	}

	@Override
	public List<Restaurant> readSearchweather(String weathertype) {
		String sql = "SELECT FoodId, Name, RNumber, Location, Type, OpenTime, loc, lng, weathertype, hotplace  FROM restaurant WHERE weathertype = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Restaurant> restaurants = new ArrayList<Restaurant>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, weathertype);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Restaurant restaurant = new Restaurant();
				restaurant.setFoodId(rs.getInt("FoodId"));
				restaurant.setName(rs.getString("Name"));
				restaurant.setNumber(rs.getString("RNumber"));
				restaurant.setLocation(rs.getString("Location"));
				restaurant.setType(rs.getString("Type"));
				restaurant.setOpen(rs.getString("OpenTime"));
				restaurant.setLatitude(rs.getString("loc"));
				restaurant.setLongitude(rs.getString("lng"));
				restaurant.setWeather(rs.getString("weathertype"));
				restaurant.setHotplace(rs.getString("hotplace"));
				restaurants.add(restaurant);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return restaurants;
	}
	@Override
	public InputStream getListPhotoById(int foodId) {
		// TODO Auto-generated method stub

		String sql =  "SELECT ResPhoto From restaurant_photo where foodId = ? limit 1";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		InputStream photo = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, foodId);
			rs = psmt.executeQuery();
			while (rs.next()) {
				photo = rs.getBinaryStream(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		
		System.out.print(photo);
		return photo;
	}
	
	@Override
	public List<NoticePost> getSoarArticles(Date today) {
		// TODO Auto-generated method stub
		String sql = " SELECT Notice_Post_TB.bno, Title, date From Notice_Post_TB inner join soar_Article on Notice_Post_TB.bno=soar_Article.bno where date = ? group by bno order by count desc limit 10";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<NoticePost> articles = new ArrayList<NoticePost>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setDate(1, today);
			rs = psmt.executeQuery();
			while (rs.next()) {
				NoticePost article = new NoticePost();
				article.setBno(rs.getInt(1));
				article.setTitle(rs.getString(2));
				article.setRegDate(rs.getDate(3));
				articles.add(article);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return articles;
	}

}
