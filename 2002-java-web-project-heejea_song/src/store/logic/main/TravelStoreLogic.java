package store.logic.main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.main.Travel;
import domain.main.TravelList;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.TravelStore;

@Repository
public class TravelStoreLogic implements TravelStore {

	private ConnectionFactory connectionFactory;
	
	
	public TravelStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	
	@Override
	public List<Travel> TravelreadByAll() {
		String sql = "SELECT travelid, name, location, loc, lng, fid from travel" ;
		
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Travel> Travels = new ArrayList<Travel>();
		
		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while(rs.next()) {
				Travel travel = new Travel();
				travel.setTravelid(rs.getInt("travelid"));
				travel.setName(rs.getString("name"));
				travel.setLocation(rs.getString("location"));
				travel.setLatitude(rs.getString("loc"));
				travel.setLongitude(rs.getString("lng"));
				travel.setFid(rs.getInt("fid"));
				Travels.add(travel);
			}
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return Travels;
	}

	@Override
	public List<Travel> TravelreadByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

}
