package store.logic.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.main.BlogPost;
import domain.main.Restaurant;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.BlogPostStore;

@Repository
public class BlogPostStoreLogic implements BlogPostStore {

	private ConnectionFactory connectionFactory;

	public BlogPostStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public List<BlogPost> readByAllBlog() {
		String sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post";
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<BlogPost> BlogPosts = new ArrayList<BlogPost>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			while (rs.next()) {
				BlogPost blog = new BlogPost();
				blog.setblogid(rs.getInt("blogid"));
				blog.setblogname(rs.getString("blogname"));
				blog.setblogtitle(rs.getString("blogtitle"));
				blog.setblogdata(rs.getString("blogdata"));
				blog.setblogbody(rs.getString("blogbody"));
				blog.setbloglink(rs.getString("link"));
				blog.setblogcount(rs.getInt("count"));
				blog.setfid(rs.getInt("fid"));
				BlogPosts.add(blog);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return BlogPosts;
	}

//	@Override
//	public List<BlogPost> readByBlogfid(Integer foodId) {
//
//		String sql = "SELECT FoodId FROM restaurant WHERE fid = ?";
//
//		Connection conn = null;
//		PreparedStatement psmt = null;
//		ResultSet rs = null;
//		Restaurant restaurant = null;
//		Statement statement = null;
//		int temper = 0;
//		List<BlogPost> BPosts = new ArrayList<BlogPost>();
//
//		try {
//			conn = connectionFactory.createConnection();
//			psmt = conn.prepareStatement(sql.toString());
//			psmt.setString(1, name);
//			rs = psmt.executeQuery();
//			if (rs.next()) {
//				restaurant = new Restaurant();
//				restaurant.setFoodId(rs.getInt(1));
//				temper = rs.getInt("FoodId");
//			}
//			JdbcUtils.close(rs, psmt, conn);
//
//			sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blogpost where fid ="
//					+ temper;
//			conn = null;
//			psmt = null;
//			rs = null;
//
//			conn = connectionFactory.createConnection();
//			statement = conn.createStatement();
//			rs = statement.executeQuery(sql);
//
//			while (rs.next()) {
//				BlogPost blog = new BlogPost();
//				blog.setblogid(rs.getInt("blogid"));
//				blog.setblogname(rs.getString("blogname"));
//				blog.setblogtitle(rs.getString("blogtitle"));
//				blog.setblogdata(rs.getString("blogdata"));
//				blog.setblogbody(rs.getString("blogbody"));
//				blog.setbloglink(rs.getString("link"));
//				blog.setblogcount(rs.getInt("count"));
//				blog.setfid(rs.getInt("fid"));
//				BPosts.add(blog);
//			}
//		} catch (SQLException e) {
//			throw new RuntimeException(e);
//		} finally {
//			JdbcUtils.close(rs, statement, conn);
//		}
//		return BPosts;
//
//	}

	@Override
	public BlogPost readcounting(String blogname) {

		String sql = "update blog_post set count = count + 1 Where blogname = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		BlogPost blogpost = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, blogname);
			psmt.executeUpdate();
			JdbcUtils.close(rs, psmt, conn);

			sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post where blogname = ?";
			conn = null;
			psmt = null;
			rs = null;
			blogpost = null;
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, blogname);
			rs = psmt.executeQuery();
			if (rs.next()) {
				blogpost = new BlogPost();
				blogpost.setblogid(rs.getInt(1));
				blogpost.setblogname(rs.getString(2));
				blogpost.setblogtitle(rs.getString(3));
				blogpost.setblogdata(rs.getString(4));
				blogpost.setblogbody(rs.getString(5));
				blogpost.setbloglink(rs.getString(6));
				blogpost.setblogcount(rs.getInt(7));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return blogpost;
	}

	@Override
	public BlogPost readblogdetail(String blogname) {
		String sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post where blogname = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		BlogPost blogpost = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, blogname);
			rs = psmt.executeQuery();
			if (rs.next()) {
				blogpost = new BlogPost();
				blogpost.setblogid(rs.getInt(1));
				blogpost.setblogname(rs.getString(2));
				blogpost.setblogtitle(rs.getString(3));
				blogpost.setblogdata(rs.getString(4));
				blogpost.setblogbody(rs.getString(5));
				blogpost.setbloglink(rs.getString(6));
				blogpost.setblogcount(rs.getInt(7));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return blogpost;
	}

	@Override
	public int readblognumber(int count) {
		
		String sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post where count = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		BlogPost blogpost = null;
		int likely = 0;
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, count);
			rs = psmt.executeQuery();
			if (rs.next()) {
				likely = rs.getInt("count");
			}
			System.out.println(blogpost.getblogtitle());
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return likely;
	}

	@Override
	public BlogPost readblogup(int blogid) {
		
		String sql = "update blog_post set count = count + 1 Where blogid = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		BlogPost blogpost = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, blogid);
			psmt.executeUpdate();
			JdbcUtils.close(rs, psmt, conn);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return blogpost;
	}

	@Override
	public BlogPost readblogdown(int blogid) {
		String sql = "update blog_post set count = count - 1 Where blogid = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		BlogPost blogpost = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, blogid);
			psmt.executeUpdate();
			JdbcUtils.close(rs, psmt, conn);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return blogpost;
	}

	@Override
	public BlogPost readblogreplace(int blogid) {
		String sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post where blogid = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		BlogPost blogpost = null;
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setInt(1, blogid);
			rs = psmt.executeQuery();
			
			if (rs.next()) {
				blogpost = new BlogPost();
				blogpost.setblogid(rs.getInt("blogid"));
				blogpost.setblogname(rs.getString("blogname"));
				blogpost.setblogtitle(rs.getString("blogtitle"));
				blogpost.setblogdata(rs.getString("blogdata"));
				blogpost.setblogbody(rs.getString("blogbody"));
				blogpost.setbloglink(rs.getString("link"));
				blogpost.setblogcount(rs.getInt("count"));
				blogpost.setfid(rs.getInt("fid"));

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return blogpost;
	}

	@Override
	public List<BlogPost> readsearchdesc() {
		String sql = "select blogid, blogname, blogtitle, blogdata, blogbody, link, count, fid from blog_post order by count desc";
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<BlogPost> BlogPosts = new ArrayList<BlogPost>();		
		
		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			while (rs.next()) {
				BlogPost blog = new BlogPost();
				blog.setblogid(rs.getInt("blogid"));
				blog.setblogname(rs.getString("blogname"));
				blog.setblogtitle(rs.getString("blogtitle"));
				blog.setblogdata(rs.getString("blogdata"));
				blog.setblogbody(rs.getString("blogbody"));
				blog.setbloglink(rs.getString("link"));
				blog.setblogcount(rs.getInt("count"));
				blog.setfid(rs.getInt("fid"));
				BlogPosts.add(blog);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return BlogPosts;
	}
}
