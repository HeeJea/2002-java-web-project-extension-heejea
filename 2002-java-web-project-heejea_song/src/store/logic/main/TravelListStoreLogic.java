package store.logic.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import domain.main.Restaurant;
import domain.main.TravelList;
import store.factory.ConnectionFactory;
import store.logic.util.JdbcUtils;
import store.main.TravelListStore;

@Repository
public class TravelListStoreLogic implements TravelListStore {

	private ConnectionFactory connectionFactory;
	
	public TravelListStoreLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	@Override
	public List<TravelList> readAllList() {
		String sql = "SELECT listid, travelname, relatelocation, loc, lng from travel_list" ;
		
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<TravelList> TravelLists = new ArrayList<TravelList>();
		
		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while(rs.next()) {
				TravelList travellist = new TravelList();
				travellist.setListid(rs.getInt("listid"));
				travellist.setName(rs.getString("travelname"));
				travellist.setRelatelist(rs.getString("relatelocation"));
				travellist.setLatitude(rs.getString("loc"));
				travellist.setLongitude(rs.getString("lng"));
				TravelLists.add(travellist);
			}
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}
		return TravelLists;
	}

	@Override
	public List<TravelList> readtypelist(String name) {
		
		String sql = "SELECT listid, travelname, relatelocation, loc, lng from travel_list WHERE travelname = ?";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<TravelList> TravelLists = new ArrayList<TravelList>();
		
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, name);
			rs = psmt.executeQuery();
			while (rs.next()) {
				TravelList travellist = new TravelList();
				travellist.setListid(rs.getInt("listid"));
				travellist.setName(rs.getString("travelname"));
				travellist.setRelatelist(rs.getString("relatelocation"));
				travellist.setLatitude(rs.getString("loc"));
				travellist.setLongitude(rs.getString("lng"));
				TravelLists.add(travellist);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return TravelLists;
	}

	@Override
	public TravelList readtraveltype(String name) {

		String sql = "SELECT listid, travelname, relatelocation from travel_list WHERE travelname = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		TravelList travellist = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, name);
			rs = psmt.executeQuery();
			if (rs.next()) {
				travellist = new TravelList();
				travellist.setListid(rs.getInt("listid"));
				travellist.setName(rs.getString("travelname"));
				travellist.setRelatelist(rs.getString("relatelocation"));
				}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return travellist;
	}

	@Override
	public TravelList readTravelcenter(String name) {
		String sql = "select listid, travelname, relatelocation, loc, lng from travel_list where travelname = ? && loc is not null";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		TravelList travellist = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, name);
			rs = psmt.executeQuery();
			if (rs.next()) {
				travellist = new TravelList();
				travellist.setListid(rs.getInt("listid"));
				travellist.setName(rs.getString("travelname"));
				travellist.setRelatelist(rs.getString("relatelocation"));
				travellist.setLatitude(rs.getString("loc"));
				travellist.setLongitude(rs.getString("lng"));
				}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}
		return travellist;

	}
	
}
