package store.main;

import java.util.List;

import domain.main.Travel;
import domain.main.TravelList;

public interface TravelStore {

	List<Travel> TravelreadByAll();
	List<Travel> TravelreadByName(String name);

}
