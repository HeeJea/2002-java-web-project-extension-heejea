package store.main;

import java.io.InputStream;
import java.sql.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import domain.main.BlogPost;
import domain.main.Restaurant;
import domain.notice.NoticePost;

public interface RestaurantStore {
	
	List<Restaurant> readByType(String type);
	List<Restaurant> readByName(String name);
	List<Restaurant> readAll();
	List<BlogPost> readBlog(Integer fooId);
	Restaurant readDetail(Integer foodId);
	Restaurant readFoodIdRestaurant(String name);
	InputStream getPhotoById(Integer foodId, Integer PhotoId);
	void setAllPhoto(String savedName, Restaurant restaurant);
	List<Restaurant> getPhotoId(Integer foodId);
	void updateSoarbyDate(Integer foodId, Date today);
	List<Restaurant> getSoarPhotoId(Date today);
	InputStream getSoarPhotoById(int foodId, Date today);
	

	List<Restaurant> readsinbo();
	List<Restaurant> readdojung();
	List<Restaurant> readbungchen();
	List<Restaurant> readbeakseok();
	List<Restaurant> readsangmyung();
	List<Restaurant> readhoseo();
	List<Restaurant> readdankook();

	InputStream getListPhotoById(int foodId);
	List<Restaurant> readSearchweather(String weathertype);
	List<NoticePost> getSoarArticles(Date today);
}
