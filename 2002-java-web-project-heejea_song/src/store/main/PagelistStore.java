package store.main;

import java.util.List;

import domain.main.Pagelist;
import domain.main.Restaurant;

public interface PagelistStore {

	List<Pagelist> readpageall();
	Pagelist readpageappend(int pageid);
	Pagelist readpagesearchid(int pageid);
	List<Restaurant> readpageidlist(int count, int fin);
}
