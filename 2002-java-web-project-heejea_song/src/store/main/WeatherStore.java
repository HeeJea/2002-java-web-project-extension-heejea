package store.main;

import java.util.List;

import domain.main.Restaurant;
import domain.main.Weather;

public interface WeatherStore {

	List<Restaurant> readAllRestaurant();
	List<Weather> readTypeClassify();
	List<Weather> readspring();
	List<Weather> readsummer();
	List<Weather> readautumn();
	List<Weather> readwinter();
	Weather readweathercheck(String weathertype); 
}
