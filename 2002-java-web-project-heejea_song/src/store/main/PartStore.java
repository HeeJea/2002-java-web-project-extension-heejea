package store.main;

import java.util.List;

import domain.main.Part;

public interface PartStore {

	List<Part> readpartall();
	Part readprogrameselect(String tvprogram);
}
