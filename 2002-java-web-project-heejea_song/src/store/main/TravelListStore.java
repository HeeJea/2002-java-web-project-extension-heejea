package store.main;

import java.util.List;

import domain.main.TravelList;

public interface TravelListStore {

	List<TravelList> readAllList();
	List<TravelList> readtypelist(String name);
	TravelList readtraveltype(String name);
	TravelList readTravelcenter(String name);
}
