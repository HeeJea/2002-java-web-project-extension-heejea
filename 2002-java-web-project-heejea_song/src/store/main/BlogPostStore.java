package store.main;

import java.util.List;

import domain.main.BlogPost;

public interface BlogPostStore {

	List<BlogPost> readByAllBlog();
//	List<BlogPost> readByBlogfid(Integer foodId);
	BlogPost readcounting(String blogname);
	BlogPost readblogdetail(String blogname);
	public int readblognumber(int count);
	BlogPost readblogup(int blogid);
	BlogPost readblogdown(int blogid);
	BlogPost readblogreplace(int blogid);
	List<BlogPost> readsearchdesc();
}
