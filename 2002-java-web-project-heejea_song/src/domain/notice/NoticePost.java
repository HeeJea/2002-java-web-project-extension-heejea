package domain.notice;

import java.io.InputStream;
import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class NoticePost {

	private int bno;		// �Խñ۹�ȣ
	private String title;	// �Խñ�����
	private String contents; // �Խñ۳���
	private String authorName;// �Խñ��ۼ���
	private Date regDate; 	// �Խñ��ۼ�����
	private int viewcnt;	// �Խñ� ��ȸ��
	
	private Integer photoId;
	private Integer[] photoIds;
    private InputStream photo;
    private MultipartFile[] files;
    private String[] files2;
    private String photoName;
	
	public int getBno() {
		return bno;
	}
    public void setBno(int bno) {
        this.bno = bno;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContents() {
        return contents;
    }
    public void setContents(String contents) {
        this.contents = contents;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public Date getRegDate() {
        return regDate;
    }
    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
    
    public int getViewcnt() {
        return viewcnt;
    }
    public void setViewcnt(int viewcnt) {
        this.viewcnt = viewcnt;
    }
	public Integer getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}
	public InputStream getPhoto() {
		return photo;
	}
	public void setPhoto(InputStream photo) {
		this.photo = photo;
	}
	public MultipartFile[] getFiles() {
		return files;
	}
	public void setFiles(MultipartFile[] files) {
		this.files = files;
	}
	public String getPhotoName() {
		return photoName;
	}
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}
	public String[] getFiles2() {
		return files2;
	}
	public void setFiles2(String[] files2) {
		this.files2 = files2;
	}
	public Integer[] getPhotoIds() {
		return photoIds;
	}
	public void setPhotoIds(Integer[] photoIds) {
		this.photoIds = photoIds;
	}
}
