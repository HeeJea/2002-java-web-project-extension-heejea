package domain.notice;

import java.sql.Date;

public class Comment {
	
	private int cno;		// ��۹�ȣ
	private String substance; //��۳���
	private String writer;// ����ۼ���
	private Date date; 	// ����ۼ�����
	
	private int bno;
	
	public int getCno() {
		return cno;
	}
    public void setCno(int cno) {
        this.cno = cno;
    }
    public String getSubstance() {
        return substance;
    }
    public void setSubstance(String substance) {
        this.substance = substance;
    }
    public String getWriter() {
        return writer;
    }
    public void setWriter(String writer) {
        this.writer = writer;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    
    public int getBno() {
		return bno;
	}
    public void setBno(int bno) {
        this.bno = bno;
    }
	
}
