package domain.main;

import java.util.List;

public class Travel {
	private int travelid;
	private String name;
	private String location;
	private String latitude;
	private String longitude;
	private int fid;
	private List<Restaurant> TravelRestaurant;
	private List<TravelList> hotplace;
	
	
	public List<Restaurant> getTravelRestaurant() {
		return TravelRestaurant;
	}
	public void setTravelRestaurant(List<Restaurant> travelRestaurant) {
		TravelRestaurant = travelRestaurant;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getTravelid() {
		return travelid;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public void setTravelid(int travelid) {
		this.travelid = travelid;
	}
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public List<TravelList> getHotplace() {
		return hotplace;
	}
	public void setHotplace(List<TravelList> hotplace) {
		this.hotplace = hotplace;
	}
	
	
}
