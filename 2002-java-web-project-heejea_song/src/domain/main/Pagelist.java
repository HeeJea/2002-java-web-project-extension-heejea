package domain.main;

import java.util.List;

public class Pagelist {
	
	private int pageid;
	private int count;
	
	public int getPageid() {
		return pageid;
	}

	public void setPageid(int pageid) {
		this.pageid = pageid;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	
	
}
