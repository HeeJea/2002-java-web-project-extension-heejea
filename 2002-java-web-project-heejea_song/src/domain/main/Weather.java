package domain.main;

import java.util.List;

public class Weather {

	private int weatherid;
	private String name;
	private String weathertype;
	private String location;
	private String body;
	
	private String latitude;
	private String longitude;
	private List<Restaurant> Restaurantlist; 
	
	public int getWeaStherid() {
		return weatherid;
	}
	public void setWeaStherid(int weatherid) {
		this.weatherid = weatherid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWeathertype() {
		return weathertype;
	}
	public void setWeathertype(String weathertype) {
		this.weathertype = weathertype;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public List<Restaurant> getRestaurantlist() {
		return Restaurantlist;
	}
	public void setRestaurantlist(List<Restaurant> restaurantlist) {
		Restaurantlist = restaurantlist;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
}
