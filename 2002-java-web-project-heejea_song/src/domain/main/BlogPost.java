package domain.main;

public class BlogPost {
	
	private int blogid;
	private String blogname;
	private String blogtitle;
	private String blogdata;
	private String blogbody;
	private String bloglink;
	private int blogcount;
	private int fid;
	
	public int getblogid() {
		return blogid;
	}

	public void setblogid(int blogid) {
		this.blogid = blogid;
	}
	
	public String getblogname() {
		return blogname;
	}

	public void setblogname(String blogname) {
		this.blogname = blogname;
	}

	public String getblogtitle() {
		return blogtitle;
	}

	public void setblogtitle(String blogtitle) {
		this.blogtitle = blogtitle;
	}

	public String getblogdata() {
		return blogdata;
	}

	public void setblogdata(String blogdata) {
		this.blogdata = blogdata;
	}

	public String getblogbody() {
		return blogbody;
	}

	public void setblogbody(String blogbody) {
		this.blogbody = blogbody;
	}

	public String getbloglink() {
		return bloglink;
	}

	public void setbloglink(String bloglink) {
		this.bloglink = bloglink;
	}
	
	public int getblogcount() {
		return blogcount;
	}

	public void setblogcount(int blogcount) {
		this.blogcount = blogcount;
	}
	public int getfid() {
		return fid;
	}

	public void setfid(int fid) {
		this.fid = fid;
	}
}
