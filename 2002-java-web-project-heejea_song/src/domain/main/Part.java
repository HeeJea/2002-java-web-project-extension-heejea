package domain.main;

public class Part {

	private int partid;
	private String restaurantname;
	private String location;
	private String tvprograme;
	private String tvdate; 
	private String mainmenu;
	
	public int getPartid() {
		return partid;
	}
	public void setPartid(int partid) {
		this.partid = partid;
	}
	public String getRestaurantname() {
		return restaurantname;
	}
	public void setRestaurantname(String restaurantname) {
		this.restaurantname = restaurantname;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTvprograme() {
		return tvprograme;
	}
	public void setTvprograme(String tvprograme) {
		this.tvprograme = tvprograme;
	}
	public String getTvdate() {
		return tvdate;
	}
	public void setTvdate(String tvdate) {
		this.tvdate = tvdate;
	}
	public String getMainmenu() {
		return mainmenu;
	}
	public void setMainmenu(String mainmenu) {
		this.mainmenu = mainmenu;
	} 
	
}
