package domain.main;

import java.io.InputStream;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class Restaurant {

	private List<BlogPost> bloglist;

	private Integer foodId;
	private String name;
	private String number;
	private String location;
	private String type;
	private String open;
	private String latitude;
	private String longitude;
	private Integer photoId;
	private InputStream photo;
	private MultipartFile[] files;
	private String weathertype;
	private String hotplace;

	public int getFoodId() {
		return foodId;
	}

	public void setFoodId(Integer foodId) {
		this.foodId = foodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public List<BlogPost> getBloglist() {
		return bloglist;
	}

	public void setBloglist(List<BlogPost> bloglist) {
		this.bloglist = bloglist;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Integer getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}

	public InputStream getPhoto() {
		return photo;
	}

	public void setPhoto(InputStream photo) {
		this.photo = photo;
	}

	public MultipartFile[] getFiles() {
		return files;
	}

	public void setFiles(MultipartFile[] files) {
		this.files = files;
	}

	public String getWeather() {
		return weathertype;
	}

	public void setWeather(String weathertype) {
		this.weathertype = weathertype;
	}

	public String getHotplace() {
		return hotplace;
	}

	public void setHotplace(String hotplace) {
		this.hotplace = hotplace;
	}

	
}
