package controller.user;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.user.User;
import service.user.UserService;

	@Controller
	@RequestMapping("User")
	public class UserController {
		
		@Autowired
		private UserService service;
		//Login 
		@RequestMapping(value = "login.do", method = RequestMethod.POST)
		public String login(Model model, HttpSession session, User user) {
			
		    User users = service.login(user);
			if(users == null || users.equals("")) {
				return "User/login";
			}
			
			if(users.getPassword().equals(user.getPassword()) && users.getId().equals(user.getId())) {
				session.setAttribute("userid", users.getId());
				session.setAttribute("userpw", users.getPassword());
				session.setAttribute("username", users.getName());
				// session.setAttribute("usermaster", users.getMaster());

				session.setAttribute("usermaster", users.getMaster());

				System.out.println(users.getMaster());
				model.addAttribute("users", this.getClass());
				
				return "redirect:/Restaurantmain/main.do";		
			}	
			else return "redirect:User/login";		
		}
		
		
		@RequestMapping(value = "login.do", method = RequestMethod.GET)
		public ModelAndView view( User user) {
			
			ModelAndView modelAndview = new ModelAndView("User/login");
			
			return modelAndview;
			
		}
		//�쉶�뜝�룞�삕 �솗�뜝�룞�삕
		//@ResponseBody
		//@RequestMapping(value = "idCheck", method = RequestMethod.POST )
		//public int postIdCheck(HttpServletRequest req) throws Exception {
			//String Id = req.getParameter(Id);
			//service.idCheck(id);
			
		
		//}
			
		
		
		@RequestMapping(value = "getUserList.do", method = RequestMethod.GET)
		public String getUserList(Model model) {
		
			List<User> list = service.getUserList();
			
			model.addAttribute("userList", list);
			
			return "User/userlist";

			} 
		
        //�쉶�뜝�룞�삕�뜝�룞�삕�뜝�룞�삕
		@RequestMapping(value = "insertUser.do", method = RequestMethod.GET)
		public String insertUser(User user){
			return "User/User_regist";
		}
		
		@RequestMapping(value = "insertUser.do", method = RequestMethod.POST)
		public String insertUser(Model model, User user){
			model.addAttribute("insertUser", service.insertUser(user));
			return "redirect:login.do";
		}
		
		//�쉶�뜝�룞�삕 �뜝�룞�삕�뜝�룞�삕
		@RequestMapping(value = "deleteUser.do", method = RequestMethod.GET)
		public String remove(User user) {
			return "User/User_delete";
		}
		
		
		@RequestMapping(value = "deleteUser.do", method = RequestMethod.POST)		
		public String remove(Model model, String id) {
			service.deleteUser(id);
//			JOptionPane.showMessageDialog(null, "占쎌돳占쎌뜚占쎌뵠 占쎄텣占쎌젫占쎈┷占쎈�占쎈뮸占쎈빍占쎈뼄.");
//			System.out.println(id);
			return "redirect:/Restaurantmain/main.do";
		}
		
		//�쉶�뜝�룞�삕 �뜝�룞�삕�뜝�룞�삕 
	@RequestMapping(value = "updateUser.do", method = RequestMethod.GET)
	public ModelAndView updateUser(@RequestParam("id")String id, HttpSession session )  {
		
		ModelAndView mav = new ModelAndView("User/updateForm"); 
		
		
		User user = service.read(id);
		
		mav.addObject("User", user);
		
		return mav;
	}

	@RequestMapping(value = "updateUser.do", method = RequestMethod.POST)
	public String updateUserlist(HttpSession session, User user) {
		
		String uid = (String)session.getAttribute("userid");
		
		System.out.println("ts");
//		List<User> list = service.getUserList();
     	service.updateUser(user);	
		
      return "redirect:mypage.do?id=" + uid;
	}
	
	@RequestMapping(value ="logout.do")
	public String logout(HttpSession session, Model model) {	
		session.invalidate();
		JOptionPane.showMessageDialog(null, "로그아웃 되었습니다.");
		return "redirect:/Restaurantmain/main.do";
	}
	@RequestMapping("mypage.do")
	public ModelAndView mypage(String id, HttpSession session)  {
		
		
		User user = service.read(id);
		
		
		ModelAndView mav = new ModelAndView("User/mypage"); 
		mav.addObject("User", user);
		
		return mav;
	}

}

