package controller.notice;

import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import domain.notice.Comment;
import domain.notice.NoticePost;
import domain.user.User;
import service.notice.NoticeBoardService;
import service.user.UserService;

@Controller
@RequestMapping("NoticeBoard")
public class NoticeBoardController {

	@Autowired
	private NoticeBoardService service;

	@Autowired
	private UserService uservice;

	@RequestMapping(value = "regist.do", method = RequestMethod.GET)
	public String regist() {
		return "noticeboard/noticeboardWrite";
	}

	@RequestMapping(value = "regist.do", method = RequestMethod.POST)
	public String regist(NoticePost noticepost, HttpSession session) throws Exception {
		noticepost.setAuthorName((String) session.getAttribute("username"));
		service.RegisterPost(noticepost);

		return "redirect:/NoticeBoard/noticeboard.do";

	}

	@RequestMapping("noticeboard.do")
	public String list(HttpSession session, Model model) {

		String uid = (String) session.getAttribute("userid");
		if (uid == null) {
			JOptionPane.showMessageDialog(null, "로그인을 해주세요.");
			return "redirect:/Restaurantmain/main.do";
		}
		User ulist = uservice.read(uid);
		List<NoticePost> list = service.retrieveAll();
		model.addAttribute("boardlist", list);
		System.out.println("ulist" + ulist.getId());
		return "noticeboard/noticeboard";
	}

	// 占쌉시글삼옙(占싹뤄옙)
	@RequestMapping("find.do")
	public String find(HttpSession session, int bno, Model model) {
		service.ReadCnt(bno);

		String uid = (String) session.getAttribute("userid");
		User ulist = uservice.read(uid);
		System.out.println("ulist" + ulist.getId());

		NoticePost noticepost = service.FindBoardPost(bno);
		model.addAttribute("post", noticepost);

		List<Comment> comlist = service.CheckAll(bno);
		model.addAttribute("comment", comlist);

		List<NoticePost> photoIds = service.findPhotoId(bno);
		model.addAttribute("photoIds", photoIds);

		service.updateSoarArticle(bno);

		return "noticeboard/noticeDetail";
	}

	@RequestMapping(value = "modify.do", method = RequestMethod.GET)
	public String modify(HttpSession session, int bno, Model model) {

		String uid = (String) session.getAttribute("userid");

		NoticePost author = service.findAuthor(bno);
		String authorId = author.getAuthorName();

		User ulist = uservice.read(uid);

		if (ulist.getName().equals(authorId)) {

			NoticePost post = service.FindBoardPost(bno);
			model.addAttribute("post", post);

			List<NoticePost> photoId = service.findPhotoName(bno);
			model.addAttribute("photoId", photoId);

			return "noticeboard/noticeModify";
		} else if (ulist.getName() != authorId) {

			JOptionPane.showMessageDialog(null, "권한이 없습니다.");
			return "redirect:/NoticeBoard/find.do?bno=" + bno;
		}

		return null;

	}

	@RequestMapping(value = "modify.do", method = RequestMethod.POST)
	public String modify(NoticePost noticepost) throws Exception {

		service.ModifyPost(noticepost);
		return "redirect:/NoticeBoard/noticeboard.do";

	}

	@RequestMapping("remove.do")
	public String remove(HttpSession session, int bno, Integer[] photoIds) {

		String uid = (String) session.getAttribute("userid");

		NoticePost author = service.findAuthor(bno);
		String authorId = author.getAuthorName();

		User ulist = uservice.read(uid);
		
		if (ulist.getName().equals(authorId)) {
			service.RemovePost(bno);
			service.removePhoto(photoIds);
			return "redirect:/NoticeBoard/noticeboard.do";
		} else if (ulist.getName() != authorId) {
			JOptionPane.showMessageDialog(null, "권한이 없습니다.");
			return "redirect:/NoticeBoard/find.do?bno=" + bno;

		}
		return null;
	}

	@RequestMapping(value = "comment.do", method = RequestMethod.POST)
	public String comment(Comment comment, HttpSession session) {

		comment.setWriter((String) session.getAttribute("username"));
		service.createComment(comment);
		// return "redirect:/Comment/find.do?bno="+comment.getBno();
		return "redirect:/NoticeBoard/find.do?bno=" + comment.getBno();
	}

	@RequestMapping(value = "update.do", method = RequestMethod.GET)
	public String update(Comment comment, Model model) {
		comment = service.Check(comment.getCno());
		model.addAttribute("comment", comment);
		return "noticeboard/comment";
	}

	@RequestMapping(value = "update.do", method = RequestMethod.POST)
	public String update(Comment comment) {

		service.updateComment(comment);
		return "redirect:/NoticeBoard/find.do?bno=" + comment.getBno();
	}

	@RequestMapping("delete.do")
	public String delete(Comment comment) {
		service.deleteComment(comment.getCno());
		return "redirect:/NoticeBoard/find.do?bno=" + comment.getBno();
	}

	@RequestMapping(value = "getphoto/{bno}/{photoId}")
	public void getphoto(@PathVariable("bno") int bno, @PathVariable("photoId") int photoId,
			HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = service.findPhotoById(bno, photoId);

		IOUtils.copy(ph, response.getOutputStream());
	}
}
