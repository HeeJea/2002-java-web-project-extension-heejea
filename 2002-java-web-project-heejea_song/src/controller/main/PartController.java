package controller.main;

import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import domain.main.BlogPost;
import domain.main.Part;
import service.main.BlogPostService;
import service.main.PartService;
import service.main.RestaurantService;

@Controller
@RequestMapping("Restaurantmain")
public class PartController {

	@Autowired
	private BlogPostService bservice;

	@Autowired
	private PartService pservice;
	
	@Autowired
	private RestaurantService rservice;
	
	@RequestMapping("parttv.do")
	public ModelAndView parttv() {
		
		List<Part> list = pservice.partall();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/parttv");
		modelAndView.addObject("PartList", list);
		
		return modelAndView;
		
	}
	
	@RequestMapping("partblog.do")
	public ModelAndView partblog() {
		
		List<BlogPost> list = bservice.searchdesc();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/partbloglist");		
		modelAndView.addObject("PartBlogList", list);
		
		return modelAndView;
		
	}
	
	@RequestMapping(value = "getphoto5/{foodId}")
	public void getphoto(@PathVariable("foodId") int foodId, HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = rservice.findListPhotoById(foodId);
		System.out.println(ph);
		IOUtils.copy(ph, response.getOutputStream());
	}

}
