package controller.main;

import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.main.BlogPost;
import domain.main.Pagelist;
import domain.main.Restaurant;
import domain.notice.NoticePost;
import service.main.PagelistService;
import service.main.RestaurantService;

@Controller
@RequestMapping("Restaurantmain")
// @RequestMapping("/home")
public class RestaurantController {
	
	@Autowired
	private RestaurantService service;

	@Autowired
	private PagelistService pservice;
	
	@RequestMapping(value = "main.do")
	public ModelAndView AllshowRestaurant() {
		
		List<Restaurant> list = service.searchAll();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/main");
		modelAndView.addObject("restaurantList", list);
		
		List<Pagelist> pnum = pservice.pageall();
		int number = pnum.get(0).getCount();
		int temp = list.size();
		
		double dtemp = (double)temp / (double)number;
		
		int temper = (int)Math.ceil(dtemp);
		
		pservice.pageappend(temper);
		System.out.println(temper);

		List<Pagelist> plist = pservice.pageall();
		modelAndView.addObject("pagelist", plist);
		
		List<Restaurant> soarphotoIds = service.findSoarPhotoId();
		modelAndView.addObject("soarphotoIds", soarphotoIds);
		
		List<NoticePost> soararticles = service.findSoarArticles();
		modelAndView.addObject("soararticles", soararticles);
		
		return modelAndView;
		
	}

	@RequestMapping(value = "pagelist.do")
	public ModelAndView pagelistcount(int pageid) {
		
		List<Restaurant> list = service.searchAll();
		List<Pagelist> plist = pservice.pageall();
		Pagelist page = pservice.pagesearchid(pageid);
		
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/main");
		modelAndView.addObject("pagelist", plist);
		
		int number = plist.get(0).getCount();
		int count = number * (pageid - 1);
		int fin = page.getCount();
		

		List<Restaurant> restpage = pservice.pageidlist(count, fin);
		modelAndView.addObject("restaurantList", restpage);
		
		
		List<Restaurant> soarphotoIds = service.findSoarPhotoId();
		modelAndView.addObject("soarphotoIds", soarphotoIds);

		
		return modelAndView;
		
	}
	
	@RequestMapping("detail.do")
	public ModelAndView findRestaurant(Integer foodId) {
//		System.out.println("hello");;
	
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/detail");
		modelAndView.addObject("detailName", service.detailByName(foodId));
		
		
		
//		re = service.SearchFoodIdRestaurant(name);
//		System.out.println(Integer.parseInt(re.toString()));
		
		
		//BlogPost blist = bservice.serachByBlogfid(re);
		
		
		//System.out.println(service.SearchFoodIdRestaurant(name));
		
				//		modelAndView.addObject("blogpostlist", blist);
		
		List<BlogPost> blist = service.findBlog(foodId);
		modelAndView.addObject("postlist", blist);
		
		List<Restaurant> photoIds = service.findPhotoId(foodId);
		modelAndView.addObject("photoIds", photoIds);
		
		service.updateSoar(foodId);
		
		return modelAndView;
		
	}
	
	
	@RequestMapping("searchByrestType.do")
	public String restbytype(Model model, @RequestParam("RestType") String Type) {
	
		List<Restaurant> rlist = service.SearchTypeRestaurant(Type);
		model.addAttribute("restaurantList", rlist);
		
		List<Restaurant> soarphotoIds = service.findSoarPhotoId();
		model.addAttribute("soarphotoIds", soarphotoIds);
		
		List<NoticePost> soararticles = service.findSoarArticles();
		model.addAttribute("soararticles", soararticles);

		return "Restaurantmain/main";
		
	}	
	
	
	@RequestMapping("searchByNameType.do")
	public String restbyName(Model model, @RequestParam("RestNameList") String name) {
	
		List<Restaurant> list = service.SearchByName(name);
		model.addAttribute("restaurantList", list);
		
		List<Restaurant> soarphotoIds = service.findSoarPhotoId();
		model.addAttribute("soarphotoIds", soarphotoIds);
		
		List<NoticePost> soararticles = service.findSoarArticles();
		model.addAttribute("soararticles", soararticles);
		
		return "Restaurantmain/main";
		
	}
	
	@RequestMapping(value = "getphoto2/{foodId}")
	public void getphoto2(@PathVariable("foodId") int foodId, HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = service.findListPhotoById(foodId);
		IOUtils.copy(ph, response.getOutputStream());
	}
	

	
	@RequestMapping(value = "photoadd.do", method = RequestMethod.GET)
	public ModelAndView photoadd(Integer foodId) {
		
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/photo");

		modelAndView.addObject("foodId", foodId);

		return modelAndView;
	}

	@RequestMapping(value = "photoadd.do", method = RequestMethod.POST)
	public String regist(Restaurant restaurant, HttpSession session) throws Exception {
		
		service.addPhoto(restaurant);

		return "redirect:/Restaurantmain/detail.do?foodId=" + restaurant.getFoodId();
	}
	
	@RequestMapping(value = "getphoto/{foodId}/{photoId}")
	public void getphoto(@PathVariable("foodId") int foodId,@PathVariable("photoId") int photoId, HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = service.findPhotoById(foodId,photoId);
		
		IOUtils.copy(ph, response.getOutputStream());
	}
	
	@RequestMapping(value = "getphoto/{foodId}")
	public void getphoto(@PathVariable("foodId") int foodId, HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = service.findSoarPhotoById(foodId);
		IOUtils.copy(ph, response.getOutputStream());
	}
	
//	@RequestMapping("test.do")
//	public String test(Model model) {
//		Restaurant rs = new Restaurant();
//		BlogPost blog = new BlogPost();
//		
//		
//		rs.setBloglist(bservice.searchByBlogAll());
//		
//		List<BlogPost> list = rs.getBloglist();
//		model.addAttribute("blog", list);
//		
//		
//		return "Restaurantmain/test";
//
//	}
	
	
//	@RequestMapping("detail2.do")
//	public ModelAndView blogt() {
//		List<BlogPost> blist = bservice.searchByBlogAll();
//
//		List<BlogPost> blist = bservice.searchByBlogAll();
//		ModelAndView modelAndView = new ModelAndView("Restaurantmain/detail");
//		modelAndView.addObject("postlist", blist);
//		return modelAndView;
//		
//	}
//	
//	
	
//	
//	
//	
//	public ModelAndView searchByRestName(@RequestParam("RestaurantName")String name) {
//		
//	}
}
