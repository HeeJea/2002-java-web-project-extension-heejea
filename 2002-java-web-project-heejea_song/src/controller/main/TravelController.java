package controller.main;

import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import domain.main.Restaurant;
import domain.main.Travel;
import domain.main.TravelList;
import service.main.RestaurantService;
import service.main.TravelListService;
import service.main.TravelService;

@Controller
@RequestMapping("Restaurantmain")
public class TravelController {

	@Autowired
	private TravelService service;
	
	@Autowired
	private TravelListService tservice;
	
	@Autowired
	private RestaurantService rservice;
	
	@RequestMapping("sinbo.do")
	public ModelAndView sinbo() {
		
		List<Restaurant> list = rservice.sinbo();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		String ts = list.get(0).getHotplace();		

		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		
		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		
		
		return modelAndView;
		
	}
	@RequestMapping("dojung.do")
	public ModelAndView dojung() {
		
		List<Restaurant> list = rservice.dojung();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		
		String ts = list.get(0).getHotplace();		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);

		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	@RequestMapping("bungchen.do")
	public ModelAndView bungchen() {
		
		List<Restaurant> list = rservice.bungchen();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		String ts = list.get(1).getHotplace();		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	@RequestMapping("beakseok.do")
	public ModelAndView beakseok() {
		
		List<Restaurant> list = rservice.beakseok();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		String ts = list.get(1).getHotplace();		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	
	@RequestMapping("sangmyung.do")
	public ModelAndView sangmyung() {
		
		List<Restaurant> list = rservice.sangmyung();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		String ts = list.get(0).getHotplace();	
		System.out.println(ts);
		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	
	@RequestMapping("hoseo.do")
	public ModelAndView hoseo() {
		
		List<Restaurant> list = rservice.hoseo();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		String ts = list.get(1).getHotplace();		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	
	@RequestMapping("dankook.do")
	public ModelAndView dankook() {
		
		List<Restaurant> list = rservice.dankook();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travel");
		modelAndView.addObject("travelList", list);
		
		String ts = list.get(1).getHotplace();		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	
	@RequestMapping("travelall.do")
	public ModelAndView travelall() {
		
		List<Restaurant> list = rservice.searchAll();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/travelall");
		modelAndView.addObject("travelList", list);
		
		
		String ts = list.get(1).getHotplace();		
		TravelList tlist = tservice.Traveltype(ts);
		modelAndView.addObject("traveltype", tlist);
		
		TravelList center = tservice.Travelcenter(ts);
		modelAndView.addObject("travelcenter", center);		

		
		modelAndView.addObject("hotplace", tservice.TypeList(ts));
		return modelAndView;
		
	}
	
	@RequestMapping(value = "getphoto3/{foodId}")
	public void getphoto(@PathVariable("foodId") int foodId, HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = rservice.findListPhotoById(foodId);
		IOUtils.copy(ph, response.getOutputStream());
	}
	
}
