package controller.main;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import domain.main.BlogPost;
import service.main.BlogPostService;


@Controller
@RequestMapping("Restaurantmain")
public class BlogPostController {
	
	@Autowired
	private BlogPostService bservice;

	@RequestMapping("blog.do")
	public ModelAndView BlogcountingAll(String blogname) {
		
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/main");	
		modelAndView.addObject("BlogList", bservice.blogCounting(blogname));
		modelAndView.addObject("BlogList", bservice.blogdetail(blogname));
		return modelAndView;
		
	}

	
//	@RequestMapping("blogup.do")
//	public void Blogmodalup(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//		
//		
//		int bnumber = Integer.parseInt(request.getParameter("blog_count"));
//		String blogtitle = request.getParameter("blogtitle");
//		BlogPost bpost = bservice.blogup(blogtitle);
//		
//		int likely = bservice.blognumber(bnumber);
//		
//		JSONObject obj = new JSONObject();
//		obj.put("like",likely); 
//		System.out.println(bnumber);
//		System.out.println(blogtitle);
//		
//		response.setContentType("application/x-json; charset=UTF-8");
//		response.getWriter().print(obj);
//		
//	}
//	
//	@RequestMapping(value="blogdown.do", method = RequestMethod.GET)
//	public String Blogmodaldown(String blogtitle) {
//		
//		return "Restaurantmain/detail.do";
//		
//	}

	@RequestMapping(value="blogup.do")
	public String Blogmodalup(int blogid, Model model) {

		System.out.println(blogid);
		bservice.blogup(blogid);
		BlogPost list = bservice.blogreplace(blogid);
		model.addAttribute("BlogList",  list);
		System.out.println(list.getfid());
		return "redirect:detail.do?foodId=" + list.getfid();
		
	}
	
	@RequestMapping(value="blogdown.do")
	public String Blogmodaldown(HttpServletRequest request, HttpServletResponse response, int blogid, Model model) throws IOException {
//		response.setContentType("text/html; charset=UTF-8");
//        PrintWriter out = response.getWriter();
//        out.println("<script>alert('클릭하셨습니다.') </script>");
//        out.flush();

		System.out.println(blogid);
		bservice.blogdown(blogid);
		BlogPost list = bservice.blogreplace(blogid);
		model.addAttribute("BlogList",  list);
		System.out.println(list.getfid());
		return "redirect:detail.do?foodId=" + list.getfid();
		
	}

}
