package controller.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import domain.main.Restaurant;
import domain.main.Weather;
import service.main.RestaurantService;
import service.main.WeatherService;


@Controller
@RequestMapping("Restaurantmain")
public class WeatherController {

	@Autowired
	private WeatherService service;
	
	@Autowired
	private RestaurantService rservice;
	
	
	@RequestMapping("weather.do")
	public ModelAndView TypeShowWeather(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		
		
		List<Weather> list = service.TypeClassify();
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/weather");
		modelAndView.addObject("spring", "봄");
		modelAndView.addObject("summer", "여름");
		modelAndView.addObject("autumn", "가을");
		modelAndView.addObject("winter", "겨울");
		
		
		modelAndView.addObject("weatherList", list);
		
		return modelAndView;
		
	}
	@RequestMapping(value = "spring.do")
	public ModelAndView spring() {
	
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/weather");
		
		List<Weather> list  =  service.spring();
		modelAndView.addObject("weatherList", list);
		return modelAndView;
		
	}
	
	@RequestMapping(value = "summer.do")
	public ModelAndView summer() {
	
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/weather");
		modelAndView.addObject("weatherList", service.summer());
		return modelAndView;
		
	}
	@RequestMapping(value = "autumn.do")
	public ModelAndView autumn() {
		
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/weather");
		modelAndView.addObject("weatherList", service.autumn());
		return modelAndView;
	}
	@RequestMapping(value = "winter.do")
	public ModelAndView winter() {
	
		ModelAndView modelAndView = new ModelAndView("Restaurantmain/weather");
		modelAndView.addObject("weatherList", service.winter());
		return modelAndView;
		
	}
	
	@RequestMapping("weatherlist.do")
	public ModelAndView weatherlist(String weathertype) {

		ModelAndView modelAndView = new ModelAndView("Restaurantmain/weatherlist");
		List<Restaurant> rlist = rservice.Searchweather(weathertype);
		modelAndView.addObject("RestaruantweatherList", rlist);
		modelAndView.addObject("weatherList", service.weathercheck(weathertype));
		
		return modelAndView;
	}
	
	@RequestMapping(value = "getphoto4/{foodId}")
	public void getphoto(@PathVariable("foodId") int foodId, HttpServletResponse response) throws Exception {
		response.setContentType("image/jpeg");

		InputStream ph = rservice.findListPhotoById(foodId);
		System.out.println(ph);
		IOUtils.copy(ph, response.getOutputStream());
	}
}

//	